import ParserTypes from '../constants/parser_types';
import ElementTypes from '../constants/element_types';
import {isArray} from '../utils/is';

/**
 * @class ElementContainer
 */
export default class ElementContainer {
	constructor() {
		this._singleLineElements = [];
		this._multiLineElements = [];
	}

	/**
	 * Returns the element definition for an element-type and a parser-type
	 * @param type {string|int} - Element type
	 * @param [parseType = PARSE_TYPE_ALL] {ParserType} - The parser-type that is set on the parser
	 * @returns {JuiElement|null}
	 * @memberOf ElementContainer
	 */
	getElementDefinition(type, parseType = ParserTypes.PARSE_TYPE_ALL) {
		if((parseType & ParserTypes.PARSE_TYPE_SINGLE_LINE) === ParserTypes.PARSE_TYPE_SINGLE_LINE) {
			const element = getFromArray(this._singleLineElements, 'types', type);

			if(element) return element;
		}

		if((parseType & ParserTypes.PARSE_TYPE_MULTI_LINE) === ParserTypes.PARSE_TYPE_MULTI_LINE) {
			return getFromArray(this._multiLineElements, 'types', type);
		}

		return null;
	}

	/**
	 * Registers a new JuiElement to the ElementContainer
	 * @param juiElement {JuiElement} - The JuiElement that should be registered
	 * @param [elementType = ELEMENT_TYPE_SINGLE_LINE] {ElementTypes} - The ElementType of the JuiElement
	 * @returns {boolean} - Item was added successfully
	 * @memberOf ElementContainer
	 */
	registerElement(juiElement, elementType = ElementTypes.ELEMENT_TYPE_SINGLE_LINE) {
		if(elementType === ElementTypes.ELEMENT_TYPE_SINGLE_LINE) {
			if(this._singleLineElements.indexOf(juiElement) !== -1) return false;

			this._singleLineElements.push(juiElement);
		} else {
			if(this._multiLineElements.indexOf(juiElement) !== -1) return false;

			this._multiLineElements.push(juiElement);
		}

		return true;
	}
}

/**
 * Returns an element from the given array that has a key with the given value or a value with an array containing the given value
 * @param array {Object[]} - Array in which should be searched
 * @param key {*} - The key which content should be compared
 * @param value {*} - Value that should be compared
 * @ignore
 * @returns {*|null}
 */
function getFromArray(array, key, value) {
	return array.find(function (element) {
		if(element[key] === value) return true;

		if(isArray(element[key]))
			return element[key].indexOf(value) !== -1;

		return false;
	}) || null;
}