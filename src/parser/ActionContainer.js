import {isString, isFunction, isArray, isObject} from '../utils/is.js';
import {getJuiProperty} from '../utils/shortener.js';

import {FUNCTION_NAME, FUNCTION_PARAMETER} from '../constants/actions';

import InvalidParameterException from '../exceptions/InvalidParameterException';

/**
 * @class ActionContainer
 */
export default class ActionContainer {

	constructor() {
		this._actionList = [];
	}

	/**
	 * Adds a new action to ActionContainer
	 * @param name {String} - The name of the action
	 * @param callback {function} - callback that should be called when the action is called
	 * @memberOf ActionContainer
	 */
	addAction(name, callback) {
		if(!isString(name)) throw new InvalidParameterException('addAction', 'name');
		if(!isFunction(callback)) throw new InvalidParameterException('addAction', 'callback');

		this._actionList.push({
			name: name.toLowerCase(),
			callback: callback
		});
	}

	/**
	 * Calls an action with the given jui-action object/array
	 * @param callObj {array|{}} - A jui-action object or an array of jui-action objects
	 * @returns {boolean} - Returns false on unimportant error
	 * @memberOf ActionContainer
	 */
	callAction(callObj) {
		if(!isObject(callObj) && !isArray(callObj)) throw new InvalidParameterException('call', 'callObj');

		if(isArray(callObj)) {
			callObj.map((action) => {
				this.callAction(action);
			});

			return true;
		}

		// assert callObj is an object
		if(!getJuiProperty(FUNCTION_NAME, callObj)) return false;

		const name = getJuiProperty(FUNCTION_NAME, callObj).toLowerCase();

		let values = getJuiProperty(FUNCTION_PARAMETER, callObj) || [];
		if(!isArray(values)) values = [values];


		for(let i = 0, x = this._actionList.length; i < x; i++) {
			if(this._actionList[i].name === name) {
				this._actionList[i].callback.apply(window, values);
				return true;
			}
		}

		return true;
	}

	/**
	 * Creates a wrapper function that prevents the event and calls the given action
	 * @param action {array|{}} - A jui-action object or an array of jui-action objects
	 * @returns {function(*)} - Function that could be used with an event listener
	 * @memberOf ActionContainer
	 */
	getCaller(action) {
		return (event) => {
			if(event && event.preventDefault)
				event.preventDefault();

			this.callAction(action);
		};
	}

	/**
	 * Searches for an action by its name
	 * @param name {String} - The action name that should be searched
	 * @returns {boolean} - Returns whether the action was found or not
	 * @memberOf ActionContainer
	 */
	hasAction(name) {
		for(let i = 0, x = this._actionList.length; i < x; i++) {
			if(this._actionList[i].name === name) {
				return true;
			}
		}

		return false;
	}
}