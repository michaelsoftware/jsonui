import {isObject, isHexColor, isNumeric, isString} from '../../utils/is.js';
import {hexToRgbString} from '../../utils/color.js';
import {getJuiProperty} from '../../utils/shortener.js';

export default class JuiDefaultElement {

	setStyle(styles) {
		if(!isObject(styles)) return;

		const margin = getJuiProperty('margin', styles);
		if(margin) this.setMargin(margin);

		const padding = getJuiProperty('padding', styles);
		if(padding) this.setPadding(padding);

		const color = getJuiProperty('color', styles);
		if(color) this.setColor(color);

		const background = getJuiProperty('background', styles);
		if(background) this.setBackground(background);

		const visibility = getJuiProperty('visibility', styles);
		if(visibility) this.setVisibility(visibility);

		const width = getJuiProperty('width', styles);
		if(width) this.setWidth(width);
	}

	setMargin(margin) {
		this.setSpace(margin, 'margin');
	}

	setPadding(padding) {
		this.setSpace(padding, 'padding');
	}

	setSpace(object, key) {
		if(!this.view || !this.view.style) return false;
		if(!isObject(object)) return false;

		const spaceAll 		= getJuiProperty('all', object);
		const spaceTop 		= getJuiProperty('top', object);
		const spaceLeft 	= getJuiProperty('left', object);
		const spaceRight 	= getJuiProperty('right', object);
		const spaceBottom 	= getJuiProperty('bottom', object);


		if(isNumeric(spaceAll)) this.view.style[key] = `${spaceAll}px`;

		if(isNumeric(spaceTop)) this.view.style[`${key}Top`] = `${spaceTop}px`;

		if(isNumeric(spaceLeft)) this.view.style[`${key}Left`] = `${spaceLeft}px`;

		if(isNumeric(spaceRight)) this.view.style[`${key}Right`] = `${spaceRight}px`;

		if(isNumeric(spaceBottom)) this.view.style[`${key}Bottom`] = `${spaceBottom}px`;

		return true;
	}

	setColor(color) {
		if(!this.view || !this.view.style) return false;
		if(!isHexColor(color, true)) return false;

		this.view.style.color = hexToRgbString(color, true);

		return true;
	}

	setBackground(background) {
		if(!this.view || !this.view.style) return false;
		if(!isHexColor(background, true)) return false;

		this.view.style.background = hexToRgbString(background, true);

		return true;
	}

	setVisibility(visibility) {
		if(!this.view || !this.view.style) return false;

		if(visibility === 'away') {
			this.view.style.display = 'none';
		} else if(visibility === 'invisible') {
			this.view.style.display = '';
			this.view.style.visibility = 'invisible';
		}

		return true;
	}

	setWidth(width) {
		if(!this.view || !this.view.style) return false;
		if(!isString(width)) return false;

		this.view.style.width = width;

		return true;
	}

	onUpdate() {
		return false;
	}

	onSubmit() {
		return null;
	}
}