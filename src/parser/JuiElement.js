import {isObject, isString} from '../utils/is.js';
import {getJuiProperty} from '../utils/shortener.js';
import InvalidParameterException from '../exceptions/InvalidParameterException';
import JuiDocument from './JuiDocument.js';

import JuiDefaultElement from './abstract/JuiDefaultElement';

/**
 * @class JuiElement
 */
export default class JuiElement extends JuiDefaultElement {
	constructor(element, juiDocument) {
		super();

		if(!isObject(element)) throw new InvalidParameterException('JuiElement', 'element');
		if(!(juiDocument instanceof JuiDocument)) throw new InvalidParameterException('JuiElement', 'juiDocument');

		this.element = element;
		this.juiDocument = juiDocument;
		this.classList = [];
		this.initialized = false;

		this.view = document.createElement('div');
	}

	/**
	 * Initializes the new element. Will automatically called by getView ( you can prevent this by calling getView(false) ).
	 * @memberOf JuiElement
	 */
	init() {
		const style = this.getValue('style');
		this.setStyle(style);

		this.initialized = true;
	}

	/**
	 * Is called from outside to request an config update. You should not override this. It's better to override the called onUpdate-function
	 * @private
	 * @param config {{}} - New config object that should be set to the element
	 * @param [append=true] {boolean} - Append the config object to the current config object (uses Object.assign)
	 * @memberOf JuiElement
	 */
	update(config, append=true) {
		if(!isObject(config)) throw new InvalidParameterException('update', 'config');

		if(append) config = Object.assign({}, this.element, config);

		const result = this.onUpdate(config);

		if(result !== false) {
			this.element = config;
		}
	}

	/**
	 * Returns the value of a key from the config-object
	 * @param key {String|int} - Short or long form of the key
	 * @returns {*|null} - Returns the value from the key or null
	 * @memberOf JuiElement
	 */
	getValue(key) {
		return getJuiProperty(key, this.element);
	}

	/**
	 * Adds a new css-class to a virtual css-class list. The classes will be appended to the view in the default getView-method.
	 * @param key {String} - CSS-class that should be added
	 * @memberOf JuiElement
	 */
	addClass(key) {
		if(!isString(key)) throw new InvalidParameterException('addClass', 'key');

		if(this.classList.indexOf(key) === -1)
			this.classList.push(key);
	}

	/**
	 * Returns the dom-view of this element
	 * @param [initialize=true] {boolean} - If set to false it will disable the auto initialization of the element
	 * @returns {Element|*}
	 * @memberOf JuiElement
	 */
	getView(initialize=true) {
		this.view.classList.add(...this.classList);

		if(initialize && !this.initialized) this.init();

		return this.view;
	}
}

JuiElement.types = null;