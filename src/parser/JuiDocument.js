import JuiElement from './JuiElement';

import InvalidParameterException from '../exceptions/InvalidParameterException';
import {isString, isArray, isObject, isFunction} from '../utils/is.js';
import {getJuiProperty} from '../utils/shortener.js';

import {FUNCTION_NAME} from '../constants/actions';

import ActionContainer from './ActionContainer';

/**
 * @class JuiDocument
 */
export default class JuiDocument {
	constructor(parser, head) {
		if(parser)
			this._parser = parser;

		if(head && isObject(head))
			this._head = head;

		this._submitViews = {};
		this._onSubmitListeners = [];
		this.actions = new ActionContainer();

		this.callAction = this.callAction.bind(this);

		this.actions.addAction('submit', this.submit.bind(this));
	}

	/**
	 * Sets the parent document of this JuiDocument
	 * @param parentDocument {JuiDocument} - The parent JuiDocument
	 */
	setParent(parentDocument) {
		if(!parentDocument || !(parentDocument instanceof JuiDocument)) throw new InvalidParameterException('setParent', 'parentDocument');
		if(parentDocument === this) throw new InvalidParameterException('setParent', 'parentDocument');

		this._parent = parentDocument;
	}

	/**
	 * Sets the view of this document
	 * @param fragment - A DOM-Fragment that should be set
	 * @memberOf JuiDocument
	 */
	setViews(fragment) {
		this._views = fragment;
	}

	/**
	 * Returns the views of the document or null
	 * @returns {*|null}
	 * @memberOf JuiDocument
	 */
	getViews() {
		return this._views || null;
	}

	/**
	 * Returns the parser that is connected to the document or null
	 * @returns {Parser|null}
	 * @memberOf JuiDocument
	 */
	getParser() {
		return this._parser || null;
	}

	/**
	 * Calls an action (the function, an action from the document or an action from the parser)
	 * @param action {function|object|array} - Action that should be called
	 * @returns {*} - Result value of the call
	 * @memberOf JuiDocument
	 */
	callAction(action) {
		if(isFunction(action)) return action();

		if(!isObject(action) && !isArray(action)) throw new InvalidParameterException('getCaller', 'action');

		const name = getJuiProperty(FUNCTION_NAME, action);

		if(this.actions.hasAction(name)) return this.actions.callAction(action);

		const parser = this.getParser();
		if(!parser) return null;

		return parser.actions.callAction(action);
	}

	/**
	 * Get all submit data from input-elements
	 * @private
	 * @returns {{}} - object with the names of the elements as keys and its values as value
	 * @memberOf JuiDocument
	 */
	getSubmitObject() {
		let submitResult = {};

		for(const key in this._submitViews) {
			if(!this._submitViews.hasOwnProperty(key)) continue;

			const element = this._submitViews[key][0];

			if(!element) continue;

			const elementResult = element.onSubmit();

			if(elementResult === null || elementResult === undefined ) continue;

			if(elementResult instanceof JuiDocument) {
				submitResult = Object.assign({}, elementResult.getSubmitObject(), submitResult);
				continue;
			}

			submitResult = Object.assign({}, {[key]: elementResult}, submitResult);
		}

		return submitResult;
	}

	/**
	 * Triggers a submit-event that reads all input-data and calls the listeners
	 * @memberOf JuiDocument
	 */
	submit() {
		if(this._parent) return this._parent.submit();

		const submitResult = this.getSubmitObject();

		this._onSubmitListeners.map((listener) => {
			listener(this, submitResult);
		});
	}

	/**
	 * Adds a submit element for a name to the document
	 * @param name {String} - The name that should be used in the submit object
	 * @param juiElement {JuiElement} - The juiElement that should be connected
	 * @memberOf JuiDocument
	 */
	addSubmitElement(name, juiElement) {
		if(!isString(name)) throw new InvalidParameterException('addSubmitElement', 'name');
		if(!(juiElement instanceof JuiElement)) throw new InvalidParameterException('addSubmitElement', 'juiElement');

		if(!this._submitViews[name]) this._submitViews[name] = [];

		this._submitViews[name].push(juiElement);
	}

	/**
	 * Registers a submit listener to the juiDocument
	 * @param callback {function} - Callback that should be registered
	 * @memberOf JuiDocument
	 */
	addOnSubmitListener(callback) {
		if(!isFunction(callback))  throw new InvalidParameterException('addOnSubmitListener', 'callback');

		if(this._parent) {
			return this._parent.addOnSubmitListener(callback);
		}

		this._onSubmitListeners.push(callback);
	}

	/**
	 * Get the head of the jsonUI
	 * @returns {{}|null} - The head of the jsonUI
	 */
	getHead() {
		return this._head || null;
	}
}