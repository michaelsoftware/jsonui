import ParserTypes from '../constants/parser_types';
import {parseJuiJSON} from '../utils/json';
import {isArray, isString} from '../utils/is.js';
import keys from '../constants/keys.js';
import ActionQueue from '../utils/ActionQueue';
import JuiDocument from './JuiDocument.js';
import JuiElement from './JuiElement.js';
import ElementContainer from './ElementContainer';
import ActionContainer from './ActionContainer';

import {registerDefaultJuiElements} from '../elements/BuildInElements';

/**
 * @class Parser
 */
export default class Parser {
	constructor() {
		this._actionQueue = new ActionQueue();
		this._actionQueue.setIterator((element) => {
			const callback = element.callback;
			const json = element.json;
			const parseType = element.parseType;

			const result = this.parseSync(json, parseType);

			callback(result);
		});

		this.elements = new ElementContainer();
		this.actions = new ActionContainer();

		registerDefaultJuiElements(this);
	}

	/**
	 * Parse jsonUI to HTML-element
	 * @param json {{}|String} - deserialized object or serialized string;
	 * @param callback {parseCallback} - Function that should be called after parsing
	 * @param [parseType = PARSE_TYPE_ALL] {ParserTypes} - Special parsing modi from parser_types constants
	 * @memberOf Parser
	 */
	parse(json, callback, parseType = ParserTypes.PARSE_TYPE_ALL) {
		this._actionQueue.add({
			json,
			callback,
			parseType
		});
	}

	/**
	 * Parse jsonUI to HTML-element in sync (you should use the async version: parse())
	 * @param json - deserialized object or serialized string;
	 * @param [parseType = PARSE_TYPE_ALL] {ParserTypes} - Special parsing modi from parser_types constants
	 * @memberOf Parser
	 */
	parseSync(json, parseType = ParserTypes.PARSE_TYPE_ALL) {
		if(isString(json)) {
			json = parseJuiJSON(json);
		}

		if(!json) return false;

		let body = json;
		if( json.body ) {
			body = json.body;
		} else if( json.data ) {
			body = json.data;
		}

		let head = json.head || {};

		let juiDocument = new JuiDocument(this, head);

		if(!body || !isArray(body)) return juiDocument;


		let fragment = document.createDocumentFragment();

		body.map((element) => {
			let el = this.parseElement(element, juiDocument, parseType);

			if(el && el instanceof JuiElement) {
				const domElement = el.getView();

				console.log('el', domElement);

				if(element.id) {
					domElement.setAttribute('id', element.id);
				}

				fragment.appendChild(domElement);
			}
		});

		juiDocument.setViews(fragment);

		return juiDocument;
	}


	/**
	 * Parses synchronously a single jsonUI-Object to an deserialized jsonUI-Element
	 * @param element {{}} - jsonUI-Object to parse
	 * @param juiDocument {JuiDocument} - JuiDocument on which the jsonUI-Elements should be registered
	 * @param [parseType = PARSE_TYPE_ALL] {int} - Special parsing modi from parser_types constants
	 * @memberOf Parser
	 */
	parseElement(element, juiDocument, parseType = ParserTypes.PARSE_TYPE_ALL) {
		const type = element[keys.type] || element['type'];

		let juiElement = this.elements.getElementDefinition(type, parseType);

		if(juiElement) {
			return new juiElement(element, juiDocument);
		}

		return new JuiElement(element, juiDocument);
	}
}


/**
 * Callback that is called when the parse-process finished
 * @callback parseCallback
 * @param {JuiDocument} juiDocument - The jui parsed as juiDocument
 * @memberOf Parser
 */