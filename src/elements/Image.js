import JuiElement from '../parser/JuiElement';

export default class Image extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('img');

		this.addClass('jui__image');

		this.view.setAttribute('src', this.getValue('value'));
	}
}

Image.types = ['image', 10];