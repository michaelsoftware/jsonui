import JuiElement from '../parser/JuiElement';

export default class Headline extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('h1');
		this.addClass('jui__headline');
	}

	getView() {
		this.view.appendChild(document.createTextNode(this.getValue('value')));

		return super.getView();
	}
}

Headline.types = ['headline', 2];