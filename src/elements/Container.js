import JuiElement from '../parser/JuiElement';

let containerId = 0;

export default class Container extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('div');
		this.addClass('jui__container');
		this.juiDocument.addSubmitElement(`__jui_container_${containerId++}`, this);
	}

	getView() {
		const parser = this.juiDocument.getParser();

		if(!parser) return super.getView();

		const juiDocument = parser.parseSync(this.getValue('value'));
		this.childrenJuiDocument = juiDocument;
		this.childrenJuiDocument.setParent(this.juiDocument);

		this.view.innerHTML = '';
		this.view.appendChild(juiDocument.getViews());

		return super.getView();
	}

	onSubmit() {
		if(!this.childrenJuiDocument) return null;

		return this.childrenJuiDocument;
	}
}

Container.types = ['container', 15];