import JuiElement from '../parser/JuiElement';

import MissingPropertyException from '../exceptions/parser/MissingPropertyException';

export default class Input extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		if(!this.getValue('name')) throw new MissingPropertyException('Input', 'name');

		this.view = document.createElement('input');
		this.addClass('jui__input');

		this.juiDocument.addSubmitElement(this.getValue('name'), this);
	}

	setValue(value) {
		this.view.setAttribute('value', value || '');
	}

	setPlaceholder(placeholder) {
		this.view.setAttribute('placeholder', placeholder || '');
	}

	getLabeledView(value, view) {
		this.label = document.createElement('label');

		this.classList.map((className) => {
			this.label.classList.add(`${className}--label`);
		});

		this.label.appendChild(document.createTextNode(value));
		this.label.appendChild(view);

		return this.label;
	}

	getView() {
		this.setValue(this.getValue('value'));
		this.setPlaceholder(this.getValue('placeholder'));

		const labelValue = this.getValue('label');

		if(labelValue) {
			return this.getLabeledView(labelValue, super.getView());
		}

		return super.getView();
	}

	onSubmit() {
		if(this.view && this.view.tagName === 'INPUT') return this.view.value;

		return super.onSubmit();
	}
}

Input.types = ['input', 5];