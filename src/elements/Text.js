import JuiElement from '../parser/JuiElement';

export default class Text extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('div');
		this.addClass('jui__text');
	}

	getView() {
		this.view.appendChild(document.createTextNode(this.getValue('value')));

		return super.getView();
	}
}

Text.types = ['text', 1];