import JuiElement from '../parser/JuiElement';
import {addStyle} from '../utils/dom';

export default class HorizontalLine extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('div');
		this.addClass('jui__line--horizontal');
		addStyle(`
			.jui__line--horizontal {
				height: 1px;
				overflow: hidden;
				background-color: #000000
			}
		`);
	}
}

HorizontalLine.types = ['hline', 4];