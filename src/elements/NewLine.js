import JuiElement from '../parser/JuiElement';

export default class NewLine extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('br');
	}
}

NewLine.types = ['nline', 3];