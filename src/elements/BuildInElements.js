import ElementTypes from '../constants/element_types';


import NewLine from './NewLine';
import HorizontalLine from './HorizontalLine';

import Headline from './Headline';
import Text from './Text';
import Input from './Input';
import NumberInput from './input/NumberInput';
import PasswordInput from './input/PasswordInput';
import Checkbox from './input/Checkbox';
import Range from './input/Range';
import Select from './input/Select';
import Button from './Button';
import File from './input/File';

import Textarea from './input/Textarea';
import List from './List';
import Container from './Container';
import Table from './table/Table';
import Image from './Image';


/**
 * @module parser/BuildInElements
 */

export {
	Headline,
	Text,
	Input,
	NumberInput,
	PasswordInput,
	Textarea,
	Checkbox,
	Range,
	Select,
	NewLine,
	HorizontalLine,
	List,
	Button,
	File,
	Container,
	Table,
	Image
};

/**
 * Registers the default-JuiElements to a parser
 * @param parser {Parser} - The parser on which the elements should be registered
 */
export function registerDefaultJuiElements(parser) {
	parser.elements.registerElement(NewLine, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(HorizontalLine, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);

	parser.elements.registerElement(Headline, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(Text, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(Input, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(NumberInput, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(PasswordInput, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(Checkbox, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(Range, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(Select, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(Button, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	parser.elements.registerElement(File, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);

	parser.elements.registerElement(Textarea, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
	parser.elements.registerElement(List, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
	parser.elements.registerElement(Container, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
	parser.elements.registerElement(Table, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
	parser.elements.registerElement(Image, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
}