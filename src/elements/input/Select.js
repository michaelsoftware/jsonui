import Input from '../Input';
import KeyShorthands from '../../constants/keys';
import {isArray, isObject, isString} from '../../utils/is';
import {getJuiProperty} from '../../utils/shortener';

export default class Select extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__select');
		this.view = document.createElement('select');
	}

	addOption(option) {
		if(isString(option)) {
			option = {
				[KeyShorthands.value]: option,
				[KeyShorthands.text]: option
			};
		}

		if(!isObject(option)) return;

		const value = getJuiProperty('value', option);
		const text = getJuiProperty('text', option);

		if(!value && !text) return;

		let tmpOption = document.createElement('option');
		tmpOption.value = getJuiProperty('value', option) || getJuiProperty('text', option);
		tmpOption.innerText = getJuiProperty('text', option);

		this.view.appendChild(tmpOption);
	}

	setValue(value) {
		if(!isArray(value)) return null;

		this.view.innerHTML = '';

		value.map((option) => {
			this.addOption(option);
		});
	}

	onSubmit() {
		return this.view.value;
	}

	setPlaceholder() {}
}

Select.types = ['select', 11];