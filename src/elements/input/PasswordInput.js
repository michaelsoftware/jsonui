import Input from '../Input';

export default class PasswordInput extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__input--password');
		this.view.setAttribute('type', 'password');
	}
}

PasswordInput.types = ['passwordinput', 17];