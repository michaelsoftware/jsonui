import Input from '../Input';

export default class NumberInput extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__input--number');
		this.view.setAttribute('type', 'number');

		this.view.value = this.getValue('value') || 0;
	}

	onSubmit() {
		let value = parseFloat(this.view.value);
		if(!value && value !== 0) value = false;

		return value;
	}
}

NumberInput.types = ['numberinput', 16];