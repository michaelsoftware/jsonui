import Input from '../Input';
import ValueShorthands from '../../constants/values';


export default class Checkbox extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__checkbox');
		this.view.setAttribute('type', 'checkbox');
	}

	setValue(checked) {
		if(checked === ValueShorthands.value.false || !checked) {
			this.view.checked = null;
		} else {
			this.view.checked = 'checked';
		}
	}

	getLabeledView(value, view) {
		this.label = document.createElement('label');

		this.classList.map((className) => {
			this.label.classList.add(`${className}--label`);
		});

		this.label.appendChild(view);
		this.label.appendChild(document.createTextNode(value));

		return this.label;
	}

	onSubmit() {
		return this.view.checked;
	}

	setPlaceholder() {}
}

Checkbox.types = ['checkbox', 7];