import Input from '../Input';
import Button from '../Button';

import LanguageHelper from '../../utils/lang/LanguageHelper';

export default class File extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__input--file');
		this.view = document.createElement('span');

		this.addViews();
	}

	addViews() {
		this.buttonElement = new Button({
			'value': this.getInputText(),
			'click': () => this.input.click()
		}, this.juiDocument);
		this.button= this.buttonElement.getView();

		this.input = document.createElement('input');
		this.input.setAttribute('type', 'file');
		this.input.style.display = 'none';
		this.input.addEventListener('change', this.onChangeFiles.bind(this));

		if(this.getValue('multiple')) {
			this.input.setAttribute('multiple', 'multiple');
		}

		this.view.appendChild( this.button );
		this.view.appendChild( this.input );
	}

	getView() {
		return super.getView();
	}

	onChangeFiles() {
		this.fileList = this.input.files;

		this.buttonElement.update({
			'value': this.getInputText()
		});
	}

	getInputText() {
		if(!this.fileList || this.fileList.length === 0) {
			let langConfig = {fileCount: 1};

			if (this.getValue('multiple')) {
				langConfig.fileCount = 2;
			}

			return LanguageHelper.get('jui.inputs.files.select_files', langConfig);
		} else {
			return LanguageHelper.get('jui.inputs.files.selected_files', {
				count: this.fileList.length
			});
		}
	}

	onSubmit() {
		return this.fileList;
	}

	setValue() {}

	setPlaceholder() {}
}

File.types = ['file', 8];