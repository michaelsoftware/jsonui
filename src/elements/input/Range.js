import Input from '../Input';

export default class Range extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__range');
		this.view.setAttribute('type', 'range');

		this.setValue(this.getValue('value'));
		this.setMin(this.getValue('min'));
		this.setMax(this.getValue('max'));
	}

	setValue(value) {
		value = parseInt(value) || 0;

		this.view.setAttribute('value', value);
	}

	setMin(min) {
		min = parseInt(min) || 0;

		this.view.setAttribute('min', min);
	}

	setMax(max) {
		max = parseInt(max) || 5;

		this.view.setAttribute('max', max);
	}

	onSubmit() {
		let value = parseInt(this.view.value);
		if(!value && value !== 0) value = false;

		return value;
	}

	setPlaceholder() {}
}

Range.types = ['range', 14];