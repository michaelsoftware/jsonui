import Input from '../Input';

export default class Textarea extends Input {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.addClass('jui__textarea');
		this.view = document.createElement('textarea');
	}

	setValue(value) {
		this.view.value = value;
	}

	onSubmit() {
		return this.view.value;
	}
}

Textarea.types = ['textarea', 18];