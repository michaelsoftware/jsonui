import JuiElement from '../../parser/JuiElement';
import {isArray} from '../../utils/is.js';

import Column from './TableColumn.js';

export default class TableRow extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('tr');
		this.addClass('jui__table--row');
	}

	getView() {
		if(!isArray(this.getValue('value'))) return super.getView();

		this.view.innerHTML = '';

		this.getValue('value').map((column) => {
			const columnElement = new Column(column, this.juiDocument);

			this.view.appendChild(columnElement.getView());
		});

		return super.getView();
	}
}