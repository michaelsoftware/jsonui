import JuiElement from '../../parser/JuiElement';
import {isArray} from '../../utils/is.js';

import Row from './TableRow.js';

export default class Table extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('table');
		this.addClass('jui__table');
	}

	getView() {
		if(!isArray(this.getValue('value'))) return super.getView();

		this.view.innerHTML = '';

		this.getValue('value').map((row) => {
			const rowElement = new Row(row, this.juiDocument);

			this.view.appendChild(rowElement.getView());
		});

		return super.getView();
	}
}

Table.types = ['table', 12];