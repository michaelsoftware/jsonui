import JuiElement from '../../parser/JuiElement';
import {isArray, isObject} from '../../utils/is.js';

let tableId = 0;

export default class TableColumn extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('td');
		this.addClass('jui__table--row');
		this.juiDocument.addSubmitElement(`__jui_table_${tableId++}`, this);
	}

	getView() {
		const parser = this.juiDocument.getParser();

		if(!isArray(this.getValue('value')) || !isObject(this.getValue('value'))) return super.getView();
		if(!parser) return super.getView();

		this.childrenJuiDocument = parser.parseSync(this.getValue('value'));
		this.childrenJuiDocument.setParent(this.juiDocument);

		this.view.innerHTML = '';
		this.view.appendChild(this.childrenJuiDocument.getViews());

		return super.getView();
	}

	onSubmit() {
		if(!this.childrenJuiDocument) return null;

		return this.childrenJuiDocument;
	}
}