import JuiElement from '../parser/JuiElement';
import {getJuiProperty} from '../utils/shortener.js';

export default class Button extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('input');
		this.view.setAttribute('type', 'button');

		this.addClass('jui__button');

		this.view.setAttribute('value', this.getValue('value'));

		this.initListeners();
	}

	initListeners() {
		const click = this.getValue('click');
		const longclick = this.getValue('longclick');

		if(click) {
			this.view.addEventListener('click', this.juiDocument.callAction.bind(this, click));
		}

		if(longclick) {
			this.view.addEventListener('dblclick', this.juiDocument.callAction.bind(this, longclick));
		}
	}

	onUpdate(newConfig) {
		if(getJuiProperty('value', newConfig) !== this.getValue('value')) {
			this.view.setAttribute('value', getJuiProperty('value', newConfig));
		}
	}

	getView() {
		return super.getView();
	}
}


Button.types = ['button', 6];