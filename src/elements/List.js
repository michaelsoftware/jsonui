import JuiElement from '../parser/JuiElement';
import constants from '../constants/constants';
import {isArray, isObject, isString} from '../utils/is';
import {getJuiProperty} from '../utils/shortener';

export default class List extends JuiElement {
	constructor(element, juiDocument) {
		super(element, juiDocument);

		this.view = document.createElement('ul');
		this.addClass('jui__list');
		this.addClass('jui__list--unordered');
	}

	addEntry(entry) {
		if(isString(entry)) {
			entry = {
				[constants.keys.value]: entry
			};
		}

		if(!isObject(entry)) return;

		const value = getJuiProperty('value', entry);

		if(!value) return;

		let tmpEntry = document.createElement('li');
		tmpEntry.appendChild(document.createTextNode(value));

		const click = getJuiProperty('click', entry);
		const longclick = getJuiProperty('longclick', entry);

		if(click) {
			tmpEntry.addEventListener('click', this.juiDocument.callAction.bind(this, click));
		}

		if(longclick) {
			tmpEntry.addEventListener('dblclick', this.juiDocument.callAction.bind(this, longclick));
		}

		this.view.appendChild(tmpEntry);
	}

	getView() {
		const value = this.getValue('value');

		if(isArray(value)) {
			this.view.innerHTML = '';

			value.map((entry) => {
				this.addEntry(entry);
			});
		}

		return super.getView();
	}
}

List.types = ['list', 9];