export default class NoRootException extends Error {
	constructor() {
		super('No root was specified to render jsonUI.');

		this.name = 'NoRootException';
		this.code = -3;

		if ('captureStackTrace' in Error)
			Error.captureStackTrace(this, NoRootException);
		else
			this.stack = (new Error()).stack;
	}
}