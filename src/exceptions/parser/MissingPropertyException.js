export default class MissingPropertyException extends Error {
	constructor(className, propertyName) {
		super(`The element (${className || '"unknown"'}) requires a ${propertyName || '"unknown"'}-property`);

		this.name = 'MissingPropertyException';
		this.code = -2;

		if ('captureStackTrace' in Error)
			Error.captureStackTrace(this, MissingPropertyException);
		else
			this.stack = (new Error()).stack;
	}
}