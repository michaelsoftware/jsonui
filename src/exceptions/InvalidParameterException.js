export default class InvalidParameterException extends Error {
	constructor(functionName, parameterName) {
		super(`Invalid parameter (${parameterName || '"unknown"'}) was passed to function ${functionName || '"unknown"'}`);

		this.name = 'InvalidParameterException';
		this.code = -2;

		if ('captureStackTrace' in Error)
			Error.captureStackTrace(this, InvalidParameterException);
		else
			this.stack = (new Error()).stack;
	}
}