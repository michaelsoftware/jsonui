export default class NotImplementedException extends Error {
	constructor(functionName) {
		super(`The method (${functionName || '"unknown"'}) is not implemented`);

		this.name = 'NotImplementedException';
		this.code = -2;

		if ('captureStackTrace' in Error)
			Error.captureStackTrace(this, NotImplementedException);
		else
			this.stack = (new Error()).stack;
	}
}