export default class CustomException extends Error {
	constructor(name, message) {
		super(message);

		this.name = name;
		this.code = -1;

		if ('captureStackTrace' in Error)
			Error.captureStackTrace(this, CustomException);
		else
			this.stack = (new Error()).stack;
	}
}