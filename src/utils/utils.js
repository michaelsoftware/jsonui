/**
 * Exports an object containing the public utils-modules
 * @module utils
 * @see ActionQueue
 * @see module:utils/is
 * @see module:utils/date
 * @see module:utils/dom
 * @see module:utils/color
 * @see module:utils/shortener
 */

import * as ActionQueue from './ActionQueue';
import * as is from './is.js';
import * as date from './date';
import * as dom from './dom';
import * as color from './color';
import * as shortener from './shortener';

const utils = {
	ActionQueue: ActionQueue,
	is: is,
	date: date,
	dom: dom,
	color: color,
	shortener: shortener
};

export default utils;