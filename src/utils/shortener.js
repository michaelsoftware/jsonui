import constants from '../constants/constants';
import {isString, isNumeric} from './is';

/**
 * @module utils/shortener
 */

/**
 * Returns a property of a jsonUI-config object
 * @param key {string, int} - key that should be searched
 * @param object {object} - jsonUI-config object
 * @returns {*|null} - value of the key or null
 */
export function getJuiProperty(key, object) {
	if(!isString(key) && !isNumeric(key)) return null;

	if(object[key] !== undefined) return object[key];

	if(constants.keys[key]) key = constants.keys[key];

	if(object[key] !== undefined) return object[key];


	for (let objKey in constants.keys) {
		if(!constants.keys.hasOwnProperty(objKey)) continue;

		if(constants.keys[objKey] == key) {
			return object[objKey];
		}
	}

	return null;
}