/**
 * @module utils/is
 */

/**
 * Tests whether an element is empty (e.g. null, '') or is a string like 'undefined', 'null'
 * @param value - Element to test
 * @returns {boolean} - True when element is empty
 */
export function isEmpty(value) {
	if(typeof value === 'undefined' || value === undefined || value === 'undefined') {
		return true;
	}

	if(value === null || value === 'null') {
		return true;
	}

	if(value === '' || value.length === 0) {
		return true;
	}

	if(value === 'NaN') {
		return true;
	}

	if(Array.isArray(value) && value.length <= 0) {
		return true;
	}

	return false;
}

/**
 * Tests whether an element is a function
 * @param obj - element to be tested
 * @returns {boolean} - Returns true when element seems to be a function
 */
export function isFunction(obj) {
	return !!(obj && obj.constructor && obj.call && obj.apply); // Thanks to: http://stackoverflow.com/questions/5999998/how-can-i-check-if-a-javascript-variable-is-function-type
}

/**
 * Tests whether an element is an object
 * @param obj - element to be tested
 * @returns {boolean} - Returns true when element is an object
 */
export function isObject(obj) {
	return obj !== null && typeof obj === 'object';
}

/**
 * Tests whether an element is an array
 * @param obj - element to be tested
 * @returns {boolean} - Returns true when element is an array
 */
export function isArray(obj) {
	if(!Array.isArray) {
		return Object.prototype.toString.call(obj) === '[object Array]';
	} else {
		return Array.isArray(obj);
	}
}

/**
 * Tests whether an element is an integer
 * @param value - element to be tested
 * @returns {boolean} - Returns true when element is an finite integer
 */
export function isInteger(value) {
	return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
}

/**
 * Tests whether an element has an numeric value (e.g. 0x539, 0.2)
 * @param value - element to be tested
 * @returns {boolean} - Returns true when element has an finite numeric value
 */
export function isNumeric(value) {
	const parsedFloat = parseFloat(value);

	return (!!parsedFloat || parsedFloat === 0) && isFinite(parsedFloat);
}

/**
 * Tests whether an element is a string
 * @param value - element to be tested
 * @returns {boolean} - Returns true when element is a string
 */
export function isString(value) {
	return typeof value === 'string' || value instanceof String;
}

/**
 * Tests whether an element is an object
 * @param value - element to be tested
 * @returns {boolean} - Returns true when element is a bool
 */
export function isBoolean(value) {
	if(value === null || value === undefined) return false;

	return typeof value === 'boolean' ||
		(typeof value === 'object' && typeof value.valueOf() === 'boolean');  // Thanks to: http://stackoverflow.com/questions/28814585/how-to-check-if-type-is-boolean
}

/**
 * Tests whether an element is a hex-color
 * @param value - element to be tested
 * @param allowAlpha - allow hexa-colors
 * @returns {boolean} - Returns true when element is a string containing a hex color
 */
export function isHexColor(value, allowAlpha) {
	const regex = allowAlpha ? /#([0-9a-f]{8}|[0-9a-f]{6}|[0-9a-f]{3})$/i : /#([0-9a-f]{6}|[0-9a-f]{3})$/i;

	return regex.test(value);
}

/**
 * Test whether an element could be a locale
 * @param input {*} - element that should be tested
 * @returns {boolean} - Returns true when element could be a locale
 */
export function isLocale(input) {
	if(!isString(input)) return false;

	return /^[a-z]{2}(_[A-Z]{2})?$/i.test(input);
}

/**
 * Tests whether an element is a regex
 * @param value {*} - element to be tested
 * @returns {boolean} - Returns true when element is a regex
 */
export function isRegex(value) {
	return (value instanceof RegExp);
}