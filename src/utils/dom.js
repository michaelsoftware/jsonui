/**
 * @module utils/dom
 */

import {isString, isFunction} from './is.js';

/**
 * Returns a dom element (if possible from value)
 * @param value - The value from which the dom-node should be readed
 * @returns {HTMLElement|null} - The DOM-Element or null
 */
export function getDomElement(value) {
	if(!value) return null;

	if(isString(value)) {
		try {
			return document.querySelector(value);
		} catch(error) {
			return null;
		}
	} else if(isFunction(value.appendChild)) {
		return value;
	}

	return null;
}

/**
 * Adds a style tag with content to the document
 * @param styleString {String} - String that represents a single or multiple css rules
 */
export function addStyle(styleString) {
	const head = document.head || document.getElementsByTagName('head')[0];
	let style = document.createElement('style');

	style.type = 'text/css';

	if (style.styleSheet){
		style.styleSheet.cssText = styleString;
	} else {
		style.appendChild(document.createTextNode(styleString));
	}

	head.appendChild(style);
}