/**
 * @module utils/json
 */

/**
 * Parse jsonUI json string. Supports error sites
 * @param data {String} - String that should be parsed
 * @returns {array|*} - Returns an array or object
 */
export function parseJuiJSON(data) {
	if(!data) {
		return [{
			type: 'heading',
			value: 'NoContent - Sorry, but there is nothing to parse :-(',
			color: '#FF0000'
		}];
	}

	const json = tryParseJSON(data);

	if(json) return json;

	return [{
		type: 'heading',
		value: 'Error while parsing JSON',
		color: '#FF0000'
	},{
		type: 'text',
		value: data
	}];
}

/**
 * Tries to parse a JSON-string
 * @param data {String} - String to be parsed
 * @returns {any|null} - Returns the deserialized JSON-string or null
 */
export function tryParseJSON(data) {
	try {
		return JSON.parse(data);
	} catch(error) {
		console.warn('Error while parsing JSON', error);

		return null;
	}
}