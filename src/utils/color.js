import {isHexColor} from './is.js';

/**
 * @module utils/color
 */

export {
	isHexColor
};

/**
 * Converts a hex-color string to a rgb-color string
 * @param hex {String} - Color represented in HEX
 * @param [allowAlpha=false] {boolean} - Allow alpha-value (HEXa)
 * @returns {String} - rgb/rgba string that represents the hex string (default: black)
 */
export function hexToRgbString(hex, allowAlpha=false) {
	const rgb = hexToRgb(hex, allowAlpha);

	if(!rgb) {
		if(allowAlpha)
			return 'rgba(0, 0, 0, 1.00)';

		return 'rgb(0, 0, 0)';
	}

	if(allowAlpha) {
		return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${(rgb.a || 0).toFixed(2)})`;
	}

	return `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
}

/**
 * Converts a hex-color string to an rgb-color object
 * @param hex {String} - Color represented in HEX
 * @param [allowAlpha=false] {boolean} - Allow alpha-value (HEXa)
 * @return {{}|null} - Object represents the rgb(a) values of the hex string
 */
export function hexToRgb(hex, allowAlpha=false) {
	if(!isHexColor(hex, allowAlpha)) return null;

	let returnValue = {};

	if(hex.length === 4) {
		returnValue.r = hex.substring(1,2) + hex.substring(1,2);
		returnValue.g = hex.substring(2,3) + hex.substring(2,3);
		returnValue.b = hex.substring(3,4) + hex.substring(3,4);
	} else if(hex.length === 7) {
		returnValue.r = hex.substring(1,3);
		returnValue.g = hex.substring(3,5);
		returnValue.b = hex.substring(5,7);
	} else if(hex.length === 9 && allowAlpha) {
		returnValue.a = hex.substring(1,3);
		returnValue.r = hex.substring(3,5);
		returnValue.g = hex.substring(5,7);
		returnValue.b = hex.substring(7,9);
	}

	returnValue.r  = parseInt(returnValue.r, 16) || 0;
	returnValue.g  = parseInt(returnValue.g, 16) || 0;
	returnValue.b  = parseInt(returnValue.b, 16) || 0;

	if(allowAlpha) {
		returnValue.a = parseInt(returnValue.a, 16);

		if(!returnValue.a && returnValue.a !== 0) returnValue.a = 255;

		returnValue.a = Math.round(returnValue.a * 100 / 255)/100;
	}

	return returnValue;
}