import {isFunction} from './is.js';

/**
 * @class ActionQueue
 */
export default class ActionQueue {
	/**
	 * Creates a new instance of an ActionQueue. It allows you to directly set an iterator-function
	 * @param [iterator] {actionIterator} - Callback that can process the items
	 * @name constructor
	 * @memberOf ActionQueue
	 */
	constructor(iterator) {
		this._queue = [];
		this._running = false;

		if(isFunction(iterator)) this._iterator = iterator;
	}

	/**
	 * Adds a new value to the ActionQueue
	 * @param value {*} - Value that should be added
	 * @memberOf ActionQueue
	 */
	add(value) {
		this._queue.push(value);

		if(!this._running) {
			next.call(this);
		}
	}

	/**
	 * Sets an iterator-function. It will override any existing iterator-functions
	 * @param iterator {actionIterator} - Callback that can process the items
	 * @returns {boolean} - Iterator was set successful
	 * @memberOf ActionQueue
	 */
	setIterator(iterator) {
		if(!isFunction(iterator)) return false;

		this._iterator = iterator;


		if(this._queue.length > 0)
			next.call(this);

		return true;
	}

	/**
	 * Tests whether the queue is currently running
	 * @returns {boolean} - Queue is running
	 * @memberOf ActionQueue
	 */
	isRunning() {
		return this._running;
	}
}

function next() {
	this._running = true;

	if (!this._iterator) {
		this._running = false;
		console.warn('No Iterator specified');
		return;
	}

	if (this._queue.length === 0) {
		this._running = false;
		return;
	}

	this._iterator(this._queue.shift());

	next.call(this);
}

/**
 * Callback function that could be used as an iterator-callback
 * @callback actionIterator
 * @param {*} value - The element that should be processed from the queue
 * @memberOf ActionQueue
 */