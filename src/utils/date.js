/**
 * @module utils/date
 */

/**
 * Returns the days the month of the year has
 * @param month {int} - The month of which the days should be returned
 * @param year {int} - The year of the month (important on february)
 * @returns {int} - The days the given month has
 */
export function getDaysInMonth(month, year) {
	if(month > 11) {
		year += Math.floor(month/12);
		month %= 12;
	}

	return [31, (isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
}

/**
 * Tests whether the given year is a leap year
 * @param year {int} - The year that should be tested
 * @returns {boolean} - The year is a leap year
 */
export function isLeapYear(year) {
	return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}