import InvalidParameterException from '../../exceptions/InvalidParameterException';
import {isString, isObject, isLocale} from '../is';

import ReplacePlugin from './ReplacePlugin.js';
import Locales from './Locale.js';


const DEFAULT_LOCALE = Locales.EN_GB;

let locale = DEFAULT_LOCALE;
let data = {};
let plugins = [];

/**
 * @class LanguageHelper
 */
export default class LanguageHelper {
	/**
	 * Returns the data to a path
	 * @param key {String} - Path that should be parsed
	 * @param [conf] {{}} - Object for plugin configuration
	 * @returns {null|String} - The string on the given path
	 * @memberOf LanguageHelper
	 */
	static get(key, conf) {
		if(conf && !isObject(conf)) throw new InvalidParameterException('get', 'conf');

		const keyPath = key.split('.');
		const keyPathLength = keyPath.length;

		if(!data[locale]) return null;

		let returnValue = data[locale];

		for(let i = 0; i < keyPathLength; i++) {
			const pathSegment = keyPath[i];

			if(!returnValue[pathSegment]) return null;

			returnValue = returnValue[pathSegment];
		}

		if(!isString(returnValue)) return null;

		plugins.map((plugin) => {
			returnValue = plugin.process(returnValue, conf);
		});

		return  returnValue;
	}

	/**
	 * Imports the data of a special collection and locale
	 * @param locale {LocaleTypes} - The locale that should be used
	 * @param collectionName {String} - Name of the collection
	 * @param collectionData {{}} - Data of the collection as object
	 * @memberOf LanguageHelper
	 */
	static loadCollectionData(locale, collectionName, collectionData) {
		if(!isLocale(locale)) throw new InvalidParameterException('loadCollectionData', 'locale');
		if(!isString(collectionName)) throw new InvalidParameterException('loadCollectionData', 'collectionName');
		if(!isObject(collectionData)) throw new InvalidParameterException('loadCollectionData', 'collectionData');

		if(!data[locale])
			data[locale] = {};

		if(!data[locale][collectionName])
			data[locale][collectionName] = collectionData;
	}

	/**
	 * Sets the locale of the user
	 * @param pLocale {LocaleTypes} - The locale that should be set on language helper
	 * @memberOf LanguageHelper
	 */
	static setLocale(pLocale) {
		if(!isLocale(pLocale)) throw new InvalidParameterException('loadCollectionData', 'locale');

		locale = pLocale;
	}

	/**
	 * Gets the current locale of the LanguageHelper
	 * @returns {string} - The currently set locale
	 * @memberOf LanguageHelper
	 */
	static getLocale() {
		return locale;
	}

	/**
	 * Clears all data from LanguageHelper
	 * @memberOf LanguageHelper
	 */
	static clear() {
		locale = DEFAULT_LOCALE;
		data = {};
		plugins = [];
	}

	/**
	 * Adds a new ReplacePlugin to the LanguageHelper
	 * @param plugin {ReplacePlugin} - plugin that should be added
	 * @memberOf LanguageHelper
	 */
	static addPlugin(plugin) {
		if(!(plugin instanceof ReplacePlugin)) throw new InvalidParameterException('addPlugin', 'plugin');

		if(plugins.indexOf(plugin) !== -1) return false;

		plugins.push(plugin);
		return true;
	}
}