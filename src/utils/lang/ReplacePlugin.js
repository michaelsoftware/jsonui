import InvalidParameterException from '../../exceptions/InvalidParameterException';
import NotImplementedException from '../../exceptions/NotImplementedException';
import {isRegex, isString, isObject} from '../is';

export default class ReplacePlugin {
	constructor(regex) {
		if(!isRegex(regex)) throw new InvalidParameterException('constructor', 'regex');

		this.regex = regex;
	}

	onFound() {
		throw new NotImplementedException('onFound');
	}

	process(string, conf) {
		if(!isString(string)) throw new InvalidParameterException('process', 'string');
		if(conf !== undefined && !isObject(conf)) throw new InvalidParameterException('process', 'conf');

		return string.replace(this.regex, (match, ...params) => {
			const replace = this.onFound(conf, match, ...params);

			if(!replace && replace !== '') return match;

			return replace;
		});
	}
}