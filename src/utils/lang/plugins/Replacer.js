import ReplacePlugin from '../ReplacePlugin';

export default class Replacer extends ReplacePlugin {
	constructor() {
		super(/##([^#]*)##/g);
	}

	onFound(conf, match, p1) {
		if(conf[p1] || conf[p1] === '') return conf[p1];
	}
}