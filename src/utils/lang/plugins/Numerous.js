import ReplacePlugin from '../ReplacePlugin';
import {isString} from '../../../utils/is.js';
import InvalidParameterException from '../../../exceptions/InvalidParameterException';

export default class Numerous extends ReplacePlugin {
	constructor() {
		super(/{([^{}?:]*)\?\?([^{}?:]*)::([^{}?:]*)}/g);

		this.keyRegex = /([^=!>< \s]+)[\s]*(==|!=|>=|<=|>|<)[\s]*([\d]+)/g;
	}

	onFound(conf, match, confKey, single, multiple) {
		let parsedConfKey = this.parseKey(confKey);

		confKey = parsedConfKey.key;
		if(!conf || conf[confKey] === undefined) return;

		const confValue = parseInt(conf[confKey]);

		if(isNaN(confValue) || !isFinite(confValue)) return;
		if(parsedConfKey.default && confValue < 0) return;

		if(this.test(parsedConfKey, confValue)) return single;

		if(multiple) return multiple;
	}

	parseKey(key) {
		if(!isString(key)) throw new InvalidParameterException('parseKey', 'key');

		const result = this.keyRegex.exec(key);

		if(!result || result.length !== 4) return {
			key: key,
			operator: '<',
			value: 2,
			default: true
		};

		const parsedKey = result[1];
		const parsedOperator = result[2];
		const parsedValue = parseInt( result[3] );

		return {
			key: parsedKey,
			operator: parsedOperator,
			value: parsedValue
		};
	}

	test(parsedConfKey, confValue) {
		const operator = parsedConfKey.operator;
		const value = parsedConfKey.value;

		switch(operator) {
			case '==': return confValue === value;
			case '!=': return confValue !== value;
			case '>=': return confValue >= value;
			case '<=': return confValue <= value;
			case '<': return confValue < value;
			case '>': return confValue > value;
			default: return false;
		}
	}
}