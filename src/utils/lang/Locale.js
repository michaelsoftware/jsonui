/**
 * Enum LocaleTypes.
 * @readonly
 * @enum {string}
 */
const LocaleTypes = {
	/** Locale for british language */
	EN_GB: 'en_GB',
	/** Locale for german language */
	DE_DE: 'de_DE'
};

export default LocaleTypes;