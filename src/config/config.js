import InvalidParameterException from '../exceptions/InvalidParameterException';
import {isObject} from '../utils/is.js';

/**
 * @module config
 */

import {initLanguageHelper} from './languageHelper';

/**
 * Initializes the jui with the given config
 * @param configObject {{}} - The config object specifies the current language
 */
export function init(configObject) {
	if(!isObject(configObject)) throw new InvalidParameterException('init', 'configObject');

	initLanguageHelper(configObject.langCode);
}