import InvalidParameterException from '../exceptions/InvalidParameterException';
import {isLocale} from '../utils/is.js';

import LanguageHelper from '../utils/lang/LanguageHelper.js';
import LocaleTypes from '../utils/lang/Locale';
import Replacer from '../utils/lang/plugins/Replacer.js';
import Numerous from '../utils/lang/plugins/Numerous.js';

import DE_DE from '../constants/lang/DE_DE';
import EN_GB from '../constants/lang/EN_GB';

/**
 * @module config/languageHelper
 */

/**
 * Initializes the LanguageHelper that is used by jui
 * @param [language=LocaleTypes.EN_GB] {LocaleTypes} - The language that should be used
 */
export function initLanguageHelper(language = LocaleTypes.EN_GB) {
	if(!isLocale(language)) throw new  InvalidParameterException('initLanguageHelper', 'language');

	LanguageHelper.clear();

	LanguageHelper.setLocale(language);

	LanguageHelper.addPlugin(new Replacer());
	LanguageHelper.addPlugin(new Numerous());
	LanguageHelper.loadCollectionData(LocaleTypes.DE_DE, 'jui', DE_DE);
	LanguageHelper.loadCollectionData(LocaleTypes.EN_GB, 'jui', EN_GB);
}