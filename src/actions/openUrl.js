import LanguageHelper from '../utils/lang/LanguageHelper';

export default function openUrl(url) {
	const win = window.open(url, '_blank');

	if (win) {
		win.focus();
	} else {
		window.alert(LanguageHelper.get('jui.actions.allow_popups'));
	}
}