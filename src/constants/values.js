export default {
	type: {
		text: 1,
		headline: 2,
		nline: 3,
		hline: 4,
		input: 5,
		button: 6,
		checkbox: 7,
		file: 8,
		list: 9,
		image: 10,
		select: 11,
		table: 12,
		frame: 13,
		range: 14,
		container: 15,
		numberinput: 16,
		passwordinput: 17,
		textarea: 18
	},
	size: {
		small: 's',
		normal: 'm',
		large: 'l'
	},
	color: {
		'#000000': 1,
		'#FFFFFF': 2,
		'#FF0000': 3,
		'#00FF00': 4,
		'#0000FF': 5,
		'#FFFF00': 6,
		'#00FFFF': 7,
		'#FF00FF': 8,
	},
	align: {
		left: 1,
		center: 2,
		right: 3
	},
	appearance: {
		bold: 1,
		italic: 2,
		bolditalic: 3
	},
	preset: {
		textarea: 1,
		password: 2,
		number: 3,
		color: 4,
		date: 5
	},
	click: {
		'submit()': 1
	},
	longclick: {
		'submit()': 1
	},
	value: {
		false: 0,
		true: 1
	}
};