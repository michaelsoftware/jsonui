/**
* Enum ParserTypes.
* @readonly
* @enum {number}
*/
const ParserTypes = {
	/** Parse only single-line elements */
	PARSE_TYPE_SINGLE_LINE: 1,
	/** Parse only multi-line elements */
	PARSE_TYPE_MULTI_LINE: 2,
	/** Parse only single-line & multi-line elements */
	PARSE_TYPE_ALL: 1 | 2
};

export default ParserTypes;