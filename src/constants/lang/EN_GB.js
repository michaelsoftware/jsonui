export default {
	inputs: {
		files: {
			select_files: 'select {fileCount??file::files}',
			selected_files: '##count## {count??file::files} selected'
		}
	},
	dialog: {
		abort: 'Abort',
		ok: 'OK'
	},
	actions: {
		allow_popups: 'Please allow popups for this website.'
	}
};