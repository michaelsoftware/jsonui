export default {
	inputs: {
		files: {
			select_files: '{fileCount??Datei::Dateien} auswählen',
			selected_files: '##count## {count??Datei::Dateien} ausgewählt'
		}
	},
	dialog: {
		abort: 'Abbrechen',
		ok: 'OK'
	},
	actions: {
		allow_popups: 'Erlauben sie bitte, dass diese Webseite Popups öffnen kann.'
	}
};