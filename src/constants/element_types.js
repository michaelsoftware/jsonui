/**
* Enum ElementTypes.
* @readonly
* @enum {number}
*/
const ElementTypes = {
	/** Element is a single-line-element */
	ELEMENT_TYPE_SINGLE_LINE: 1,
	/** Element is a multi-line-element */
	ELEMENT_TYPE_MULTI_LINE: 2
};

export default ElementTypes;