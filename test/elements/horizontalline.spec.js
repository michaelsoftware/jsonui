import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import HorizontalLine from '../../src/elements/HorizontalLine.js';

describe("elements/HorizontalLine", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new HorizontalLine() });

		expect(() => { new HorizontalLine({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new HorizontalLine(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {};
		let juiElement =  new HorizontalLine(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('tagName').equal('DIV');
		expect(juiElement.getView()).to.have.property('className').equal('jui__line--horizontal');
	});

	it('types', function() {
		expect(HorizontalLine).to.have.own.property('types').to.be.an('array');

		expect(HorizontalLine).to.have.own.property('types').includes('hline');
		expect(HorizontalLine).to.have.own.property('types').includes(4);
	});
});

