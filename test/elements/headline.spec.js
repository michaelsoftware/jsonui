import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import Headline from '../../src/elements/Headline.js';

describe("elements/Headline", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Headline() });

		expect(() => { new Headline({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new Headline(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'Hello World!'
		};
		let juiElement =  new Headline(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('innerText').equal('Hello World!');
		expect(juiElement.getView()).to.have.property('tagName').equal('H1');
		expect(juiElement.getView()).to.have.property('className').equal('jui__headline');
	});

	it('types', function() {
		expect(Headline).to.have.own.property('types').to.be.an('array');

		expect(Headline).to.have.own.property('types').includes('headline');
		expect(Headline).to.have.own.property('types').includes(2);
	});
});

