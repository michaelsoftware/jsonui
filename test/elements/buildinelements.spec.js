import ElementTypes from '../../src/constants/element_types';

import {Headline, Text, NewLine, HorizontalLine, Button} from '../../src/elements/BuildInElements';
import {Input, NumberInput, PasswordInput, Textarea, Checkbox, Range, Select, File} from '../../src/elements/BuildInElements';
import {List, Container, Table, Image} from '../../src/elements/BuildInElements';

import {registerDefaultJuiElements} from '../../src/elements/BuildInElements';

describe("elements/BuildInElements", function() {
	beforeEach(function() { });
	afterEach(function() { });




	const parser_spy = {

		singleLineElements: [],
		multiLineElements: [],

		elements: {
			registerElement: function (def, type) {
				if(type === ElementTypes.ELEMENT_TYPE_SINGLE_LINE) {
					parser_spy.singleLineElements.push(def);
				}

				if(type === ElementTypes.ELEMENT_TYPE_MULTI_LINE) {
					parser_spy.multiLineElements.push(def);
				}

				return true;
			}
		}
	};

	it('registerDefaultJuiElements', function() {
		registerDefaultJuiElements(parser_spy);

		expect(parser_spy.singleLineElements).to.have.lengthOf(12);
		expect(parser_spy.multiLineElements).to.have.lengthOf(5);


		expect(parser_spy.singleLineElements).to.include(NewLine);
		expect(parser_spy.singleLineElements).to.include(HorizontalLine);

		expect(parser_spy.singleLineElements).to.include(Headline);
		expect(parser_spy.singleLineElements).to.include(Text);
		expect(parser_spy.singleLineElements).to.include(Input);
		expect(parser_spy.singleLineElements).to.include(NumberInput);
		expect(parser_spy.singleLineElements).to.include(PasswordInput);
		expect(parser_spy.singleLineElements).to.include(Checkbox);
		expect(parser_spy.singleLineElements).to.include(Range);
		expect(parser_spy.singleLineElements).to.include(Select);
		expect(parser_spy.singleLineElements).to.include(Button);
		expect(parser_spy.singleLineElements).to.include(File);

		expect(parser_spy.multiLineElements).to.include(Textarea);
		expect(parser_spy.multiLineElements).to.include(List);
		expect(parser_spy.multiLineElements).to.include(Container);
		expect(parser_spy.multiLineElements).to.include(Table);
		expect(parser_spy.multiLineElements).to.include(Image);
	});
});

