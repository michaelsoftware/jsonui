import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import Input from '../../src/elements/Input.js';

describe("elements/Input", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Input() });

		expect(() => { new Input({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new Input(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			value: 'Hello World!',
			'11': 'Test',
			name: 'test'
		};
		let juiElement =  new Input(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('value').equal('Hello World!');
		expect(juiElement.getView()).to.have.property('placeholder').equal('Test');
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input');
	});

	it('getView (label)', function() {
		let juiDocument = new JuiDocument();

		let element = document.createElement('div');
		element.appendChild(document.createTextNode('hello_element'));

		const confObj = {
			'11': 'Test',
			name: 'test',
			label: 'label_test'
		};
		let juiElement =  new Input(confObj, juiDocument);
		juiElement.getLabeledView = function(value, view) {
			expect(value).to.equal('label_test');
			expect(view).to.equal(juiElement.view);

			return element;
		};

		expect(juiElement.getView()).to.equal(element);
	});

	it('setValue', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			type: 'test_input',
			name: 'test_input'
		};
		let juiElement =  new Input(confObj, juiDocument);

		function getSetAttributeSpy(expectName, expectValue) {
			return {
				setAttribute: (name, value) => {
					expect(name).to.equal(expectName);
					expect(value).to.equal(expectValue);
				}
			}
		}


		juiElement.view = getSetAttributeSpy('value', 'test_value');
		juiElement.setValue('test_value');

		juiElement.view = getSetAttributeSpy('value', '');
		juiElement.setValue();
	});

	it('setPlaceholder', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			type: 'test_input',
			name: 'test_input'
		};
		let juiElement =  new Input(confObj, juiDocument);

		function getSetAttributeSpy(expectName, expectValue) {
			return {
				setAttribute: (name, value) => {
					expect(name).to.equal(expectName);
					expect(value).to.equal(expectValue);
				}
			}
		}


		juiElement.view = getSetAttributeSpy('placeholder', 'test_value');
		juiElement.setPlaceholder('test_value');

		juiElement.view = getSetAttributeSpy('placeholder', '');
		juiElement.setPlaceholder();
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			name: 'test'
		};
		let juiElement =  new Input(confObj, juiDocument);

		expect(juiElement.onSubmit()).to.equal('');

		juiElement.view.value = 'value_test';
		expect(juiElement.onSubmit()).to.equal('value_test');

		juiElement.view = {tagName: 'SELECT'};
		expect(juiElement.onSubmit()).to.equal(null);
	});

	it('getLabeledView', function() {
		let juiDocument = new JuiDocument();

		let element = document.createElement('div');
		element.appendChild(document.createTextNode('test_element'));

		const confObj = {
			name: 'test_name'
		};
		let juiElement = new Input(confObj, juiDocument);
		expect(juiElement.getLabeledView('test_label', element)).to.have.own.property('childNodes').with.lengthOf(2);

		expect(juiElement.getLabeledView('test_label', element).childNodes[0]).to.have.own.property('nodeType').to.equal(3);
		expect(juiElement.getLabeledView('test_label', element).childNodes[0]).to.have.own.property('nodeValue').to.equal('test_label');

		expect(juiElement.getLabeledView('test_label', element).childNodes[1]).to.equal(element);

		juiElement.classList = ['test_class_1', 'test_class_2'];
		expect(juiElement.getLabeledView('test_label', element)).to.have.own.property('className').to.equal('test_class_1--label test_class_2--label');
	});

	it('types', function() {
		expect(Input).to.have.own.property('types').to.be.an('array');

		expect(Input).to.have.own.property('types').includes('input');
		expect(Input).to.have.own.property('types').includes(5);
	});
});

