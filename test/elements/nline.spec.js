import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import NewLine from '../../src/elements/NewLine.js';

describe("elements/NewLine", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new NewLine() });

		expect(() => { new NewLine({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new NewLine(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {};
		let juiElement =  new NewLine(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('tagName').equal('BR');
	});

	it('types', function() {
		expect(NewLine).to.have.own.property('types').to.be.an('array');

		expect(NewLine).to.have.own.property('types').includes('nline');
		expect(NewLine).to.have.own.property('types').includes(3);
	});
});

