import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import Image from '../../src/elements/Image.js';

describe("elements/Image", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Image() });

		expect(() => { new Image({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new Image(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = { 2: 'http://example.com/' };
		let juiElement =  new Image(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('tagName').equal('IMG');
		expect(juiElement.getView()).to.have.property('src').equal('http://example.com/');
	});

	it('types', function() {
		expect(Image).to.have.own.property('types').to.be.an('array');

		expect(Image).to.have.own.property('types').includes('image');
		expect(Image).to.have.own.property('types').includes(10);
	});
});

