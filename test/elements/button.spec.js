import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import Button from '../../src/elements/Button.js';

describe("elements/Button", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Button() });

		expect(() => { new Button({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new Button(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'Hello World!'
		};
		let juiElement =  new Button(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('value').equal('Hello World!');
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__button');
	});

	it('types', function() {
		expect(Button).to.have.own.property('types').to.be.an('array');

		expect(Button).to.have.own.property('types').includes('button');
		expect(Button).to.have.own.property('types').includes(6);
	});
});

