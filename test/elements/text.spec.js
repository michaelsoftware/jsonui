import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import Text from '../../src/elements/Text.js';

describe("elements/Text", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Text() });

		expect(() => { new Text({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new Text(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'Hello World!'
		};
		let juiElement =  new Text(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('innerText').equal('Hello World!');
		expect(juiElement.getView()).to.have.property('tagName').equal('DIV');
		expect(juiElement.getView()).to.have.property('className').equal('jui__text');
	});

	it('types', function() {
		expect(Text).to.have.own.property('types').to.be.an('array');

		expect(Text).to.have.own.property('types').includes('text');
		expect(Text).to.have.own.property('types').includes(1);
	});
});

