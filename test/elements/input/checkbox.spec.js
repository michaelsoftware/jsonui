import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import Checkbox from '../../../src/elements/input/Checkbox.js';

describe("elements/input/Checkbox", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Checkbox() });

		expect(() => { new Checkbox({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new Checkbox(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView_1', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'true',
			name: 'test'
		};
		let juiElement =  new Checkbox(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView()).to.have.property('checked').equal(true);
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__checkbox');
	});

	it('getView_2', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': false,
			name: 'test'
		};
		let juiElement =  new Checkbox(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView()).to.have.property('checked').equal(false);
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__checkbox');
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new Checkbox({ name: 'test' }, juiDocument);

		expect(juiElement.onSubmit()).to.to.equal(false);

		juiElement.view.checked = true;
		expect(juiElement.onSubmit()).to.to.equal(true);
	});

	it('types', function() {
		expect(Checkbox).to.have.own.property('types').to.be.an('array');

		expect(Checkbox).to.have.own.property('types').includes('checkbox');
		expect(Checkbox).to.have.own.property('types').includes(7);
	});
});

