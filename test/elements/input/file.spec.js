import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import File from '../../../src/elements/input/File.js';
import constants from '../../../src/constants/constants';

describe("elements/input/File", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let submitRegisterCalls = 0;

		let juiDocument = new JuiDocument();
		juiDocument.addSubmitElement = (name, juiElement) => {
			submitRegisterCalls++;
			expect(name).to.equal('test');
		};

		function expectError(func, errorName='InvalidParameterException') {
			expect(func).to.throw(Error).to.have.property('name').to.equal(errorName);
		}

		expectError(() => { new File() });

		expectError(() => { new File({}, juiDocument) }, 'MissingPropertyException');

		expect(submitRegisterCalls).to.equal(0);
		expect(() => { new File({name: 'test'}, juiDocument) }).to.not.throw();
		expect(submitRegisterCalls).to.equal(1);
		submitRegisterCalls = 0;

		const confObj = {name: 'test'};
		let juiElement =  new File(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);

		expect(juiElement).to.have.property('input').to.be.not.equal(null);
		expect(juiElement).to.have.property('button').to.be.not.equal(null);

		expect(submitRegisterCalls).to.equal(1);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			[constants.keys.type]: constants.values.type.file,
			[constants.keys.name]: 'test'
		};
		let juiElement = new File(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView()).to.have.property('children').to.have.lengthOf(2);

		const children = juiElement.getView().children;
		const button = children[0];
		const input = children[1];

		expect(button).to.have.property('tagName').equal('INPUT');
		expect(button).to.have.property('value').to.equal('select file');
		expect(button).to.have.property('type').equal('button');

		expect(input).to.have.property('tagName').equal('INPUT');
		expect(input).to.have.property('type').equal('file');

		expect(juiElement.getView()).to.have.property('tagName').equal('SPAN');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__input--file');
	});

	it('getInputText (single)', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			[constants.keys.type]: constants.values.type.file,
			[constants.keys.name]: 'test'
		};
		let juiElement = new File(confObj, juiDocument);

		expect(juiElement.getInputText()).to.equal('select file');

		juiElement.fileList = [];
		expect(juiElement.getInputText()).to.equal('select file');

		juiElement.fileList = ['file 1'];
		expect(juiElement.getInputText()).to.equal('1 file selected');

		juiElement.fileList = ['file 1', 'file_2'];
		expect(juiElement.getInputText()).to.equal('2 files selected');
	});

	it('getInputText (multiple)', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			[constants.keys.type]: constants.values.type.file,
			[constants.keys.name]: 'test',
			[constants.keys.multiple]: true
		};
		let juiElement = new File(confObj, juiDocument);

		expect(juiElement.getInputText()).to.equal('select files');

		juiElement.fileList = [];
		expect(juiElement.getInputText()).to.equal('select files');

		juiElement.fileList = ['file 1'];
		expect(juiElement.getInputText()).to.equal('1 file selected');

		juiElement.fileList = ['file 1', 'file_2'];
		expect(juiElement.getInputText()).to.equal('2 files selected');
	});

	it('onChangeFiles', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			[constants.keys.type]: constants.values.type.file,
			[constants.keys.name]: 'test'
		};
		let juiElement = new File(confObj, juiDocument);

		juiElement.getInputText = () => 'test string';
		juiElement.onChangeFiles();
		expect(juiElement).to.have.property('button').to.have.property('value').to.equal('test string');
	});

	it('types', function() {
		expect(File).to.have.own.property('types').to.be.an('array');

		expect(File).to.have.own.property('types').includes('file');
		expect(File).to.have.own.property('types').includes(8);
	});
});

