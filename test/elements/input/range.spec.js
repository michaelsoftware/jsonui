import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import Range from '../../../src/elements/input/Range.js';

describe("elements/input/Range", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Range() });

		expect(() => { new Range({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new Range(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new Range({ name: 'test' }, juiDocument);

		expect(juiElement.onSubmit()).to.to.equal(0);

		juiElement.view = {value: 'test'};
		expect(juiElement.onSubmit()).to.to.equal(false);

		juiElement.view = {value: '-15'};
		expect(juiElement.onSubmit()).to.to.equal(-15);

		juiElement.view = {value: '0'};
		expect(juiElement.onSubmit()).to.to.equal(0);

		juiElement.view = {value: '100'};
		expect(juiElement.onSubmit()).to.to.equal(100);
	});

	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': '16',
			'17': '-1',
			'18': '20',
			name: 'test'
		};
		let juiElement =  new Range(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView().value).to.equal('16');
		expect(juiElement.getView().getAttribute('min')).to.equal('-1');
		expect(juiElement.getView().getAttribute('max')).to.equal('20');
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__range');
	});

	it('types', function() {
		expect(Range).to.have.own.property('types').to.be.an('array');

		expect(Range).to.have.own.property('types').includes('range');
		expect(Range).to.have.own.property('types').includes(14);
	});
});

