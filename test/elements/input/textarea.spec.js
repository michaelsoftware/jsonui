import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import Textarea from '../../../src/elements/input/Textarea.js';

describe("elements/input/Textarea", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Textarea() });

		expect(() => { new Textarea({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new Textarea(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'Hello World!',
			'11': 'Test',
			name: 'test'
		};
		let juiElement =  new Textarea(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView().value).to.equal('Hello World!');
		expect(juiElement.getView().getAttribute('placeholder')).to.equal('Test');
		expect(juiElement.getView()).to.have.property('tagName').equal('TEXTAREA');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__textarea');
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new Textarea({ name: 'test' }, juiDocument);

		expect(juiElement.onSubmit()).to.to.equal('');

		juiElement.view.value = 'hello_test_value';
		expect(juiElement.onSubmit()).to.to.equal('hello_test_value');
	});

	it('types', function() {
		expect(Textarea).to.have.own.property('types').to.be.an('array');

		expect(Textarea).to.have.own.property('types').includes('textarea');
		expect(Textarea).to.have.own.property('types').includes(18);
	});
});

