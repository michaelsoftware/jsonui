import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import PasswordInput from '../../../src/elements/input/PasswordInput.js';

describe("elements/input/PasswordInput", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new PasswordInput() });

		expect(() => { new PasswordInput({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new PasswordInput(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'Hello World!',
			'11': 'Test',
			name: 'test'
		};
		let juiElement =  new PasswordInput(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView().getAttribute('value')).to.equal('Hello World!');
		expect(juiElement.getView().getAttribute('placeholder')).to.equal('Test');
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__input--password');
	});

	it('types', function() {
		expect(PasswordInput).to.have.own.property('types').to.be.an('array');

		expect(PasswordInput).to.have.own.property('types').includes('passwordinput');
		expect(PasswordInput).to.have.own.property('types').includes(17);
	});
});

