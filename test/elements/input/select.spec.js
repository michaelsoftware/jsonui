import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import Select from '../../../src/elements/input/Select.js';
import constants from '../../../src/constants/constants';

describe("elements/input/Select", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new Select() });

		expect(() => { new Select({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new Select(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			[constants.keys.type]: constants.values.type.select,
			[constants.keys.name]: 'test',
			[constants.keys.value]: [{
				[constants.keys.value]: 'test',
				[constants.keys.text]: 'Hello World!'
			},
				"Single String",{
					[constants.keys.text]: 'Only text'
				},
				{}]
		};
		let juiElement =  new Select(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView()).to.have.property('children').to.have.lengthOf(3);

		const options = juiElement.getView().children;

		expect(options).to.have.lengthOf(3);

		expect(options[0].value).to.equal('test');
		expect(options[0].innerText).to.equal('Hello World!');

		expect(options[1].value).to.equal('Single String');
		expect(options[1].innerText).to.equal('Single String');

		expect(options[2].value).to.equal('Only text');
		expect(options[2].innerText).to.equal('Only text');

		expect(juiElement.getView()).to.have.property('tagName').equal('SELECT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__select');
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new Select({ name: 'test' }, juiDocument);

		expect(juiElement.onSubmit()).to.to.equal('');

		juiElement.view = {value: 'test_value'};
		expect(juiElement.onSubmit()).to.to.equal('test_value');
	});

	it('types', function() {
		expect(Select).to.have.own.property('types').to.be.an('array');

		expect(Select).to.have.own.property('types').includes('select');
		expect(Select).to.have.own.property('types').includes(11);
	});
});

