import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import NumberInput from '../../../src/elements/input/NumberInput.js';

describe("elements/input/NumberInput", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new NumberInput() });

		expect(() => { new NumberInput({name: 'test'}, juiDocument) }).to.not.throw();

		const confObj = {name: 'test'};
		let juiElement =  new NumberInput(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': 'Hello World!',
			'11': 'Test',
			name: 'test'
		};
		let juiElement =  new NumberInput(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView().getAttribute('value')).to.equal('Hello World!');
		expect(juiElement.getView().getAttribute('placeholder')).to.equal('Test');
		expect(juiElement.getView()).to.have.property('tagName').equal('INPUT');
		expect(juiElement.getView()).to.have.property('className').equal('jui__input jui__input--number');
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new NumberInput({ name: 'test' }, juiDocument);

		expect(juiElement.onSubmit()).to.to.equal(0);

		juiElement.view = {value: 'test'};
		expect(juiElement.onSubmit()).to.to.equal(false);

		juiElement.view = {value: '-15'};
		expect(juiElement.onSubmit()).to.to.equal(-15);

		juiElement.view = {value: '0'};
		expect(juiElement.onSubmit()).to.to.equal(0);

		juiElement.view = {value: '100'};
		expect(juiElement.onSubmit()).to.to.equal(100);
	});

	it('types', function() {
		expect(NumberInput).to.have.own.property('types').to.be.an('array');

		expect(NumberInput).to.have.own.property('types').includes('numberinput');
		expect(NumberInput).to.have.own.property('types').includes(16);
	});
});

