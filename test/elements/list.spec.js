import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import List from '../../src/elements/List.js';
import constants from '../../src/constants/constants';

describe("elements/input/List", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new List() });

		expect(() => { new List({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new List(confObj, juiDocument);
		expect(juiElement).to.be.not.null;
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('classList').to.be.an('array');
		expect(juiElement).to.have.property('view').to.be.not.equal(null);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			[constants.keys.value]: [{
					[constants.keys.value]: 'test'
				},
				"Single String",
				{}]
		};
		let juiElement =  new List(confObj, juiDocument);

		expect(juiElement.getView()).to.be.not.null;
		expect(juiElement.getView()).to.have.property('tagName').equal('UL');
		expect(juiElement.getView()).to.have.property('className').equal('jui__list jui__list--unordered');
		expect(juiElement.getView()).to.have.property('children').to.have.lengthOf(2);
		expect(juiElement.getView().children[0]).to.have.property('innerText').to.equal('test');
		expect(juiElement.getView().children[1]).to.have.property('innerText').to.equal('Single String');
	});

	it('types', function() {
		expect(List).to.have.own.property('types').to.be.an('array');

		expect(List).to.have.own.property('types').includes('list');
		expect(List).to.have.own.property('types').includes(9);
	});
});

