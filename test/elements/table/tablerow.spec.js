import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import TableRow from '../../../src/elements/table/TableRow.js';


describe("elements/TableRow", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new TableRow() });

		expect(() => { new TableRow({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new TableRow(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
		expect(juiElement).to.have.property('view').to.have.property('tagName').to.equal('TR');
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();

		const confObj = {
			'2': [
				{2: 'test_1'},
				{2: 'test_2'}
			]
		};
		let juiElement =  new TableRow(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('children').lengthOf(2);
		expect(juiElement.getView().children[0]).to.have.property('tagName').to.equal('TD');
		expect(juiElement.getView().children[1]).to.have.property('tagName').to.equal('TD');
	});

	it('types', function() {
		expect(TableRow).to.not.have.own.property('types');
	});
});

