import InvalidParameterException from '../../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../../src/parser/JuiDocument.js';
import TableColumn from '../../../src/elements/table/TableColumn.js';

describe("elements/TableColumn", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new TableColumn() });

		expect(() => { new TableColumn({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new TableColumn(confObj, juiDocument);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
		expect(juiElement).to.have.property('view').to.have.property('tagName').to.equal('TD');
	});


	it('getView', function() {
		let juiDocument = new JuiDocument({
			parseSync: (values) => {
				return {
					getViews: () => {
						let fragment = document.createDocumentFragment();

						values.map((value) => {
							let element = document.createElement('div');
							element.appendChild(document.createTextNode(value));

							fragment.appendChild(element);
						});

						return fragment;
					},
					setParent: (doc) => {
						expect(doc).to.equal(juiDocument);
					}
				};
			}
		});

		const confObj = {
			'2': ['test_1', 'test_2']
		};
		let juiElement =  new TableColumn(confObj, juiDocument);

		expect(juiElement.getView()).to.have.property('children').lengthOf(2);
		expect(juiElement.getView().children[0]).to.have.property('innerText').to.equal('test_1');
		expect(juiElement.getView().children[1]).to.have.property('innerText').to.equal('test_2');
	});

	it('onSubmit', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new TableColumn({ }, juiDocument);

		expect(juiElement.onSubmit()).to.to.equal(null);

		juiElement.childrenJuiDocument = 'test_children_jui_document';
		expect(juiElement.onSubmit()).to.to.equal('test_children_jui_document');
	});

	it('types', function() {
		expect(TableColumn).to.not.have.own.property('types');
	});
});

