import JuiDefaultElement from '../../../src/parser/abstract/JuiDefaultElement.js';

describe("parser/JuiDefaultElement", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		expect(() => new JuiDefaultElement()).to.not.throw();
	});

	it('setStyle_1', function() {
		let juiElement = new JuiDefaultElement();

		let calls = { margin: 0, padding: 0, color: 0, background: 0, visibility: 0, width: 0 };
		const style = {
			margin: 'test_margin',
			padding: 'test_padding',
			color: 'test_color',
			background: 'test_background',
			visibility: 'test_visibility',
			width: 'test_width'
		};

		juiElement.setMargin = (margin) => { expect(margin).to.equal('test_margin'); calls.margin++; };
		juiElement.setPadding = (padding) => { expect(padding).to.equal('test_padding'); calls.padding++; };
		juiElement.setColor = (color) => { expect(color).to.equal('test_color'); calls.color++; };
		juiElement.setBackground = (background) => { expect(background).to.equal('test_background'); calls.background++; };
		juiElement.setVisibility = (visibility) => { expect(visibility).to.equal('test_visibility'); calls.visibility++; };
		juiElement.setWidth = (width) => { expect(width).to.equal('test_width'); calls.width++; };

		juiElement.setStyle(style);

		expect(calls.margin).to.equal(1);
		expect(calls.padding).to.equal(1);
		expect(calls.color).to.equal(1);
		expect(calls.background).to.equal(1);
		expect(calls.visibility).to.equal(1);
		expect(calls.width).to.equal(1);
	});

	it('setStyle_2', function() {
		let juiElement = new JuiDefaultElement();

		let calls = { margin: 0, padding: 0, color: 0, background: 0, visibility: 0, width: 0 };
		const style = {
			margin: 'test_margin',
			color: 'test_color',
			visibility: 'test_visibility'
		};

		juiElement.setMargin = (margin) => { expect(margin).to.equal('test_margin'); calls.margin++; };
		juiElement.setPadding = (padding) => { expect(padding).to.equal('test_padding'); calls.padding++; };
		juiElement.setColor = (color) => { expect(color).to.equal('test_color'); calls.color++; };
		juiElement.setBackground = (background) => { expect(background).to.equal('test_background'); calls.background++; };
		juiElement.setVisibility = (visibility) => { expect(visibility).to.equal('test_visibility'); calls.visibility++; };
		juiElement.setWidth = (width) => { expect(width).to.equal('test_visibility'); calls.width++; };


		juiElement.setStyle(null);
		expect(calls.margin).to.equal(0);
		expect(calls.padding).to.equal(0);
		expect(calls.color).to.equal(0);
		expect(calls.background).to.equal(0);
		expect(calls.visibility).to.equal(0);
		expect(calls.width).to.equal(0);


		juiElement.setStyle(style);
		expect(calls.margin).to.equal(1);
		expect(calls.padding).to.equal(0);
		expect(calls.color).to.equal(1);
		expect(calls.background).to.equal(0);
		expect(calls.visibility).to.equal(1);
		expect(calls.width).to.equal(0);
	});

	it('setMargin', function() {
		let juiElement =  new JuiDefaultElement();

		juiElement.setSpace = (object, key) => {
			expect(object).to.equal('test_margin');
			expect(key).to.equal('margin');
		};

		juiElement.setMargin('test_margin');
	});

	it('setPadding', function() {
		let juiElement =  new JuiDefaultElement();

		juiElement.setSpace = (object, key) => {
			expect(object).to.equal('test_padding');
			expect(key).to.equal('padding');
		};

		juiElement.setPadding('test_padding');
	});

	it('setSpace', function() {
		let juiElement =  new JuiDefaultElement();
		juiElement.view = { style: {} };

		const testSpaces = {
			all: 1,
			top: 2,
			left: 3,
			right: '4',
			bottom: 'hjgg'
		};

		const setSpaceResult = juiElement.setSpace(testSpaces, 'padding');
		expect(setSpaceResult).to.equal(true);

		expect(juiElement.view.style).to.have.own.property('padding').to.equal('1px');
		expect(juiElement.view.style).to.have.own.property('paddingTop').to.equal('2px');
		expect(juiElement.view.style).to.have.own.property('paddingLeft').to.equal('3px');
		expect(juiElement.view.style).to.have.own.property('paddingRight').to.equal('4px');
		expect(juiElement.view.style).to.not.have.property('paddingBottom');

		expect(juiElement.setSpace()).to.equal(false);

		juiElement.view.style = null;
		expect(juiElement.setSpace({})).to.equal(false);

		juiElement.view = null;
		expect(juiElement.setSpace({})).to.equal(false);
	});

	it('setColor', function() {
		let juiElement =  new JuiDefaultElement();

		expect(juiElement.setColor()).to.equal(false);
		expect(juiElement.setColor("test")).to.equal(false);

		juiElement.view = { style:{} };
		const setColorResult = juiElement.setColor("#112233");

		expect(setColorResult).to.equal(true);
		expect(juiElement.view.style).to.have.own.property('color').to.equal('rgba(17, 34, 51, 1.00)');
	});

	it('setBackground', function() {
		let juiElement =  new JuiDefaultElement();

		expect(juiElement.setColor()).to.equal(false);
		expect(juiElement.setColor("test")).to.equal(false);

		juiElement.view = { style:{} };
		const setColorResult = juiElement.setBackground("#80445566");

		expect(setColorResult).to.equal(true);
		expect(juiElement.view.style).to.have.own.property('background').to.equal('rgba(68, 85, 102, 0.50)');
	});

	it('setVisibility', function() {
		let juiElement =  new JuiDefaultElement();

		expect(juiElement.setVisibility()).to.equal(false);
		expect(juiElement.setVisibility("test")).to.equal(false);

		juiElement.view = { style:{} };
		const setColorResult = juiElement.setVisibility("#80445566");
		expect(setColorResult).to.equal(true);
		expect(juiElement.view.style).to.not.have.property('display');
		expect(juiElement.view.style).to.not.have.property('visibility');


		const setColorResult2 = juiElement.setVisibility("away");
		expect(setColorResult2).to.equal(true);
		expect(juiElement.view.style).to.have.property('display').to.equal('none');
		expect(juiElement.view.style).to.not.have.property('visibility');

		const setColorResult3 = juiElement.setVisibility("invisible");
		expect(setColorResult3).to.equal(true);
		expect(juiElement.view.style).to.have.property('display').to.not.equal('none');
		expect(juiElement.view.style).to.have.property('visibility').to.equal('invisible');
	});

	it('setWidth', function() {
		let juiElement =  new JuiDefaultElement();

		expect(juiElement.setWidth()).to.equal(false);
		expect(juiElement.setWidth("test")).to.equal(false);

		juiElement.view = { style:{} };
		const setColorResult = juiElement.setWidth("test_width");
		expect(setColorResult).to.equal(true);
		expect(juiElement.view.style).to.have.own.property('width').to.equal('test_width');
	});

	it('onUpdate', function() {
		let juiElement =  new JuiDefaultElement();

		expect(juiElement.onUpdate()).to.equal(false);
	});

	it('onSubmit', function() {
		let juiElement =  new JuiDefaultElement();

		expect(juiElement.onSubmit()).to.equal(null);
	});
});

