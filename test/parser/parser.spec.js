import Parser from '../../src/parser/Parser.js';
import JuiDocument from '../../src/parser/JuiDocument.js';
import ElementContainer from '../../src/parser/ElementContainer.js';
import ActionContainer from '../../src/parser/ActionContainer.js';
import JuiElement from '../../src/parser/JuiElement.js';
import ActionQueue from '../../src/utils/ActionQueue';
import ParseTypes from '../../src/constants/parser_types';

describe("parser/Parser", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('empty constructor', function() {
		let parser = new Parser();

		expect(parser).to.have.property('actions').to.be.an.instanceOf(ActionContainer);
		expect(parser).to.have.property('elements').to.be.an.instanceOf(ElementContainer);
		expect(parser).to.have.property('_actionQueue').to.be.an.instanceOf(ActionQueue);
	});



	let testArray = [{
		callback: (result) => {
		},
		juiDocument: {
			body: [{
				type: "headline",
				value: "Hello World"
			}]
		},
		type: ParseTypes.PARSE_TYPE_ALL
	},{
		callback: (result) => {
		},
		juiDocument: {
			body: [{
				type: "headline",
				value: "Hello World"
			}]
		},
		type: ParseTypes.PARSE_TYPE_SINGLE_LINE
	}];

	it('parse', function(done) {
		let parser = new Parser();

		let i = 0;

		parser._actionQueue = {
			add: function(element) {

				expect(element).to.have.property('json').to.equal(testArray[i].juiDocument);
				expect(element).to.have.property('callback').to.equal(testArray[i].callback);
				expect(element).to.have.property('parseType').to.equal(testArray[i].type);

				if(i >= 1) {
					done();
				}

				i++;
			}
		};

		parser.parse(testArray[0].juiDocument, testArray[0].callback);
		parser.parse(testArray[1].juiDocument, testArray[1].callback, testArray[1].type);
	});


	it('parseElement', function() {
		let parser = new Parser();
		let juiDocument = new JuiDocument();

		parser.elements = {
			i: 0,
			getElementDefinition: function(type, parseType) {
				if(this.i === 0) {
					this.i++;

					expect(type).to.equal('headline');
					expect(parseType).to.equal(ParseTypes.PARSE_TYPE_ALL);

					return this.constructor_1;
				} else if(this.i === 1) {
					this.i++;

					expect(type).to.equal('headline');
					expect(parseType).to.equal(ParseTypes.PARSE_TYPE_SINGLE_LINE);

					return null;
				}
			},
			constructor_1: function(pElement, pJuiDocument) {
				expect(pElement).to.equal(testArray[0].juiDocument.body[0]);
				expect(pJuiDocument).to.equal(juiDocument);
			}
		};

		const elem_1 = parser.parseElement(testArray[0].juiDocument.body[0], juiDocument);
		expect(elem_1).to.be.instanceOf(parser.elements.constructor_1);

		const elem_2 = parser.parseElement(testArray[1].juiDocument.body[0], juiDocument, ParseTypes.PARSE_TYPE_SINGLE_LINE);
		expect(elem_2).to.be.instanceOf(JuiElement);

		expect(parser.elements.i).to.equal(2);
	});

	it('parseSync', function() {
		let parser = new Parser();

		parser.parseElement = function(element, juiDocument) {
			return new JuiElement(element, juiDocument);
		};

		{
			const result = parser.parseSync(testArray[0].juiDocument);

			expect(result).to.be.instanceOf(JuiDocument);

			expect(result.getViews()).to.be.not.null;

			const tmpDom = document.createElement('div');
			tmpDom.appendChild(result.getViews());

			expect(tmpDom).to.have.property('children').to.have.lengthOf(1);
			expect(tmpDom.children).to.have.property('0').to.have.property('tagName').to.equal('DIV');
		}


		{
			const result2 = parser.parseSync({});

			expect(result2).to.be.instanceOf(JuiDocument);
			expect(result2.getViews()).to.equal(null);
		}

		{
			const head = {test_type: 'jui_head'};
			const result3 = parser.parseSync({head: head});

			expect(result3).to.be.instanceOf(JuiDocument);
			expect(result3.getViews()).to.equal(null);
			expect(result3.getHead()).to.equal(head);
		}
	});
});

