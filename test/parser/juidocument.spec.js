import JuiDocument from '../../src/parser/JuiDocument.js';
import JuiElement from '../../src/parser/JuiElement.js';
import ActionContainer from '../../src/parser/ActionContainer.js';

import {FUNCTION_NAME} from '../../src/constants/actions';

describe("parser/JuiDocument", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('empty constructor', function() {
		let juiDocument = new JuiDocument();
		expect(juiDocument).to.be.not.null;
		expect(juiDocument).to.have.own.property('_submitViews').to.be.an('object').that.is.empty;
		expect(juiDocument).to.have.own.property('_onSubmitListeners').to.be.an('array').that.is.empty;
		expect(juiDocument).to.have.own.property('actions').isPrototypeOf(ActionContainer);
		expect(juiDocument).to.not.have.property('_parent');
	});

	it('constructor', function() {
		const parser = { test_type: 'parser' };
		const head = { test_type: 'head' };
		const parent = new JuiDocument();

		const juiDocument_parser = new JuiDocument(parser);
		expect(juiDocument_parser).to.have.own.property('_parser').to.equal(parser);
		expect(juiDocument_parser).to.not.have.property('_head');

		const juiDocument_head = new JuiDocument(null, head);
		expect(juiDocument_head).to.not.have.property('_parser');
		expect(juiDocument_head).to.have.own.property('_head').to.equal(head);

		const juiDocument_inv_parser = new JuiDocument(null, "bad_type");
		expect(juiDocument_inv_parser).to.not.have.property('_parser');
		expect(juiDocument_inv_parser).to.not.have.property('_head');

		const juiDocument_parser_head = new JuiDocument(parser, head);
		expect(juiDocument_parser_head).to.have.own.property('_parser').to.equal(parser);
		expect(juiDocument_parser_head).to.have.own.property('_head').to.equal(head);

		const juiDocument_parser_head_parent = new JuiDocument(parser, head, parent);
		expect(juiDocument_parser_head_parent).to.have.own.property('_parser').to.equal(parser);
		expect(juiDocument_parser_head_parent).to.have.own.property('_head').to.equal(head);
	});

	it('set parent', function() {
		let juiDocument = new JuiDocument();
		let parentJuiDocument = new JuiDocument();

		expect(() => juiDocument.setParent()).to.throw().to.have.property('name').to.equal('InvalidParameterException');
		expect(() => juiDocument.setParent({})).to.throw().to.have.property('name').to.equal('InvalidParameterException');
		expect(() => juiDocument.setParent(juiDocument)).to.throw().to.have.property('name').to.equal('InvalidParameterException');
		expect(juiDocument).to.not.have.property('_parent');

		juiDocument.setParent(parentJuiDocument);
		expect(juiDocument).to.have.own.property('_parent').to.equal(parentJuiDocument);
	});

	it('set views', function() {
		let juiDocument = new JuiDocument();

		const fragment = document.createDocumentFragment();
		fragment.appendChild(document.createTextNode('Hello World!'));
		fragment.appendChild(document.createTextNode('Test'));

		juiDocument.setViews(fragment);

		expect(juiDocument).to.have.property('_views').to.equal(fragment);
	});


	it('get views', function() {
		let juiDocument = new JuiDocument();

		const fragment = document.createDocumentFragment();
		fragment.appendChild(document.createTextNode('Hello World!'));
		fragment.appendChild(document.createTextNode('Test'));

		juiDocument.setViews(fragment);

		juiDocument.setViews(document.createDocumentFragment());

		expect(juiDocument.getViews()).to.not.equal(fragment);

		juiDocument.setViews(fragment);

		expect(juiDocument.getViews()).to.equal(fragment);
	});

	it('addSubmitElement', function() {
		let juiDocument = new JuiDocument();

		function expectError(func, errorName='InvalidParameterException') {
			expect(func).to.throw(Error).to.have.property('name').to.equal(errorName);
		}

		expectError(() => juiDocument.addSubmitElement());

		expectError(() => juiDocument.addSubmitElement('', null));

		const element_1 = new JuiElement({testId:1}, juiDocument);
		const element_2 = new JuiElement({testId:2}, juiDocument);
		const element_3 = new JuiElement({testId:3}, juiDocument);

		juiDocument.addSubmitElement('test', element_1);
		expect(juiDocument).to.have.property('_submitViews').to.have.own.property('test').to.be.an('array').lengthOf(1).to.contain(element_1);

		juiDocument.addSubmitElement('test_2', element_2);
		expect(juiDocument).to.have.property('_submitViews').to.have.own.property('test').to.be.an('array').lengthOf(1).to.contain(element_1);
		expect(juiDocument).to.have.property('_submitViews').to.have.own.property('test_2').to.be.an('array').lengthOf(1).to.contain(element_2);

		juiDocument.addSubmitElement('test', element_3);
		expect(juiDocument).to.have.property('_submitViews').to.have.own.property('test').to.be.an('array').lengthOf(2).to.contain(element_1).and.to.contain(element_3);
		expect(juiDocument).to.have.property('_submitViews').to.have.own.property('test_2').to.be.an('array').lengthOf(1).to.contain(element_2);
	});

	it('addOnSubmitListener', function() {
		let juiDocument = new JuiDocument();

		juiDocument._onSubmitListeners = [];

		function expectError(func, errorName='InvalidParameterException') {
			expect(func).to.throw(Error).to.have.property('name').to.equal(errorName);
		}

		expectError(() => juiDocument.addOnSubmitListener());

		expectError(() => juiDocument.addOnSubmitListener(''));

		expect(juiDocument).to.have.own.property('_onSubmitListeners').with.lengthOf(0);

		const function_1 = () => {};
		const function_2 = () => {};
		const function_3 = () => {};

		juiDocument.addOnSubmitListener(function_1);
		juiDocument.addOnSubmitListener(function_2);
		juiDocument.addOnSubmitListener(function_3);

		expect(juiDocument).to.have.own.property('_onSubmitListeners').with.lengthOf(3);
		expect(juiDocument._onSubmitListeners[0]).to.equal(function_1);
		expect(juiDocument._onSubmitListeners[1]).to.equal(function_2);
		expect(juiDocument._onSubmitListeners[2]).to.equal(function_3);
	});

	it('submit', function() {
		let juiDocument = new JuiDocument();
		let submitListenerCallCount = 0;

		juiDocument._submitViews = {
			test: [{
				onSubmit: () => 'Hello Test!'
			}],
			test_2: [{
				onSubmit: () => 'Hello Test_2!'
			},{
				onSubmit: () => 'Hello Test_2_2!'
			}],
			test_3: [{
				onSubmit: () => null
			}]
		};

		juiDocument._onSubmitListeners.push((juiDocumentParam, submitResult) => {
			expect(juiDocumentParam).to.equal(juiDocument);

			expect(submitResult).to.have.own.property('test').to.equal('Hello Test!');
			expect(submitResult).to.have.own.property('test_2').to.equal('Hello Test_2!');
			expect(submitResult).to.not.have.own.property('test_3');

			submitListenerCallCount++;
		});

		juiDocument.submit();

		expect(submitListenerCallCount).to.equal(1);
	});

	it('submit (juiDocument)', function() {
		let juiDocument = new JuiDocument();
		let submitJuiDocument = new JuiDocument();
		submitJuiDocument.getSubmitObject = () => { return { test: 'hello' } };

		let submitListenerCallCount = 0;

		juiDocument._submitViews = {
			test_1: [{
				onSubmit: () => submitJuiDocument
			}],
			test_2: [{
				onSubmit: () => 'test'
			}]
		};

		juiDocument._onSubmitListeners.push((juiDocumentParam, submitResult) => {
			expect(juiDocumentParam).to.equal(juiDocument);

			expect(submitResult).to.have.own.property('test').to.equal('hello');
			expect(submitResult).to.have.own.property('test_2').to.equal('test');
			expect(submitResult).to.not.have.own.property('test_1');

			submitListenerCallCount++;
		});

		juiDocument.submit();

		expect(submitListenerCallCount).to.equal(1);
	});

	it('submit (parent)', function() {
		let submitCallCount = 0, getSubmitObjectCallCount = 0;
		let juiDocument = new JuiDocument();

		juiDocument._parent = {
			submit: () => {
				submitCallCount++;
			}
		};

		juiDocument.getSubmitObject = () => {
			getSubmitObjectCallCount++;
		};


		juiDocument.submit();

		expect(submitCallCount).to.equal(1);
		expect(getSubmitObjectCallCount).to.equal(0);
	});

	it('callAction', function() {
		let juiDocument = new JuiDocument();

		const testFunction = function() { return 'test_function_result' };
		expect(juiDocument.callAction(testFunction)).to.equal('test_function_result');

		const testAction = {
			[FUNCTION_NAME]: 'test_function_name'
		};

		juiDocument.actions = {
			hasAction: (name) => {
				expect(name).to.equal('test_function_name');

				return true;
			},
			callAction: (action) => {
				expect(action).to.equal(testAction);

				return true;
			}
		};
		expect(juiDocument.callAction(testAction)).to.equal(true);
	});

	it('callAction (parse action)', function() {
		let juiDocument = new JuiDocument();

		const testAction = {
			[FUNCTION_NAME]: 'test_function_name'
		};

		juiDocument.actions = {
			hasAction: (name) => false
		};
		juiDocument._parser = {
			actions: {
				callAction: (action) => {
					expect(action).to.equal(testAction);
					return true;
				}
			}
		};
		expect(juiDocument.callAction(testAction)).to.equal(true);
	});

	it('getHead', function() {
		const head = { test_type: 'head' };
		let juiDocument = new JuiDocument();

		expect(juiDocument.getHead()).to.equal(null);

		juiDocument._head = "";
		expect(juiDocument.getHead()).to.equal(null);

		juiDocument._head = head;
		expect(juiDocument.getHead()).to.equal(head);
	});
});

