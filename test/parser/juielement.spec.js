import InvalidParameterException from '../../src/exceptions/InvalidParameterException';
import JuiDocument from '../../src/parser/JuiDocument.js';
import JuiElement from '../../src/parser/JuiElement.js';

describe("parser/JuiElement", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let juiDocument = new JuiDocument();

		function expectError(func) {
			expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
		}

		expectError(() => { new JuiElement() });
		expectError(() => { new JuiElement({}) });
		expectError(() => { new JuiElement({}, 1) });
		expectError(() => { new JuiElement("wrong", juiDocument) });

		expect(() => { new JuiElement({}, juiDocument) }).to.not.throw();

		const confObj = {};
		let juiElement =  new JuiElement(confObj, juiDocument);
		expect(juiElement).to.have.property('initialized').to.equal(false);
		expect(juiElement).to.have.property('element').to.equal(confObj);
		expect(juiElement).to.have.property('juiDocument').to.equal(juiDocument);
		expect(juiElement).to.have.property('view').to.not.equal(null);
		expect(juiElement).to.have.property('classList').to.be.an('array');
	});

	it('init', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new JuiElement({ style: 'test_style' }, juiDocument);

		let setStyleCount = 0;
		juiElement.setStyle = (style) => { expect(style).to.equal('test_style'); setStyleCount++ };

		juiElement.init();

		expect(setStyleCount).to.equal(1);
		expect(juiElement).to.have.own.property('initialized').to.equal(true);
	});

	it('getValue', function() {
		let juiDocument = new JuiDocument();


		const colorObj = {
			'visibility': 'none'
		};
		const confObj = {
			'type':'1', // type:text
			'1':'text', // type:text
			'2':'Hello World!', // value:Hello World!
			'html':'<h1>Test</h1>',
			'15': colorObj
		};
		let juiElement =  new JuiElement(confObj, juiDocument);

		expect(juiElement.getValue('type')).to.equal('1');
		expect(juiElement.getValue('1')).to.equal('text');

		expect(juiElement.getValue('2')).to.equal('Hello World!');
		expect(juiElement.getValue('value')).to.equal('Hello World!');

		expect(juiElement.getValue('16')).to.equal('<h1>Test</h1>');
		expect(juiElement.getValue('html')).to.equal('<h1>Test</h1>');

		expect(juiElement.getValue('15')).to.equal(colorObj);
	});


	it('getView', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new JuiElement({}, juiDocument);

		expect(juiElement.getView()).to.not.equal(null);

		const view = document.createElement('h1');
		juiElement.view = view;

		let initialize = 0;
		juiElement.init = () => {initialize++};
		juiElement.initialized = false;

		expect(juiElement.getView()).to.equal(view);
		expect(initialize).to.equal(1);


		initialize = 0;
		juiElement.initialized = false;

		expect(juiElement.getView(false)).to.equal(view);
		expect(initialize).to.equal(0);
	});

	it('addClass', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new JuiElement({}, juiDocument);

		juiElement.addClass('test');

		expect(juiElement).to.have.property('classList').to.have.lengthOf(1).to.contain('test');

		juiElement.addClass('test2');

		expect(juiElement).to.have.property('classList').to.have.lengthOf(2).to.contain('test2');

		expect(() => juiElement.addClass({})).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');

		expect(juiElement).to.have.property('classList').to.have.lengthOf(2);

		expect(juiElement.getView()).to.have.property('className').equal('test test2');
	});

	it('update', function() {
		let juiDocument = new JuiDocument();
		let juiElement =  new JuiElement({}, juiDocument);

		expect(() => juiElement.update()).to.throw().to.have.property('name').to.equal('InvalidParameterException');
		expect(() => juiElement.update("test")).to.throw().to.have.property('name').to.equal('InvalidParameterException');

		juiElement.onUpdate = () => true;
		const newConfig = {
			test1: 'new property 1'
		};
		juiElement.update(newConfig);
		expect(juiElement).to.have.own.property('element').to.containSubset(newConfig);


		juiElement.onUpdate = () => false;
		const newConfig_2 = {
			test2: 'new property 2'
		};
		juiElement.update(newConfig_2);
		expect(juiElement).to.have.own.property('element').to.containSubset(newConfig);


		juiElement.onUpdate = () => false;
		const newConfig_3 = {
			test3: 'new property 3'
		};
		juiElement.update(newConfig_3, true);
		expect(juiElement).to.have.own.property('element').to.containSubset(newConfig);


		juiElement.onUpdate = () => true;
		const newConfig_4 = {
			test4: 'new property 4'
		};
		juiElement.update(newConfig_4, true);
		expect(juiElement).to.have.own.property('element').to.have.own.property('test1').to.equal('new property 1');
		expect(juiElement).to.have.own.property('element').to.have.own.property('test4').to.equal('new property 4');
	});
});

