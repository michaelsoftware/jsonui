import ActionContainer from '../../src/parser/ActionContainer.js';
import {FUNCTION_NAME, FUNCTION_PARAMETER} from '../../src/constants/actions';

describe("parser/ActionContainer", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let actionContainer = new ActionContainer();

		expect(actionContainer).to.have.property('_actionList').to.be.an('array').to.be.empty;
	});

	function expectError(func) {
		expect(func).to.throw(Error).to.have.property('name').to.equal('InvalidParameterException');
	}

	it('addAction', function() {
		let actionContainer = new ActionContainer();

		expectError(() => actionContainer.addAction());
		expectError(() => actionContainer.addAction(""));
		expectError(() => actionContainer.addAction("", {}));

		expect(actionContainer).to.have.property('_actionList').to.be.an('array').to.be.empty;

		let testFunction_1 = () => {};
		actionContainer.addAction("test_1", testFunction_1);

		expect(actionContainer).to.have.property('_actionList').to.be.an('array').with.lengthOf(1);
		expect(actionContainer._actionList[0]).to.have.own.property('name').to.equal('test_1');
		expect(actionContainer._actionList[0]).to.have.own.property('callback').to.equal(testFunction_1);


		let testFunction_2 = () => {};
		actionContainer.addAction("test_2", testFunction_2);

		expect(actionContainer).to.have.property('_actionList').to.be.an('array').with.lengthOf(2);
		expect(actionContainer._actionList[0]).to.have.own.property('name').to.equal('test_1');
		expect(actionContainer._actionList[0]).to.have.own.property('callback').to.equal(testFunction_1);
		expect(actionContainer._actionList[1]).to.have.own.property('name').to.equal('test_2');
		expect(actionContainer._actionList[1]).to.have.own.property('callback').to.equal(testFunction_2);
	});

	it('hasAction', function() {
		let actionContainer = new ActionContainer();
		actionContainer._actionList.push({ name: 'test_1' });
		actionContainer._actionList.push({ name: 'test_2' });
		actionContainer._actionList.push({ name: 1 });

		expect(actionContainer.hasAction('')).to.equal(false);
		expect(actionContainer.hasAction('1')).to.equal(false);
		expect(actionContainer.hasAction(null)).to.equal(false);

		expect(actionContainer.hasAction('test_1')).to.equal(true);
		expect(actionContainer.hasAction('test_2')).to.equal(true);
		expect(actionContainer.hasAction(1)).to.equal(true);
	});


	it('callAction/single', function() {
		let actionContainer = new ActionContainer();

		expectError(() => actionContainer.callAction());
		expectError(() => actionContainer.callAction(""));

		let actionCallCount = 0;
		actionContainer._actionList.push({
			name: 'test',
			callback: (param) => {
				expect(param).to.equal('test_parameter');
				actionCallCount++;
			}
		});

		actionContainer.callAction({
			[FUNCTION_NAME]: 'test',
			[FUNCTION_PARAMETER]: 'test_parameter'
		});
		expect(actionCallCount).to.equal(1);

		actionCallCount = 0;
		actionContainer.callAction({
			[FUNCTION_NAME]: 'test',
			[FUNCTION_PARAMETER]: ['test_parameter']
		});
		expect(actionCallCount).to.equal(1);
	});

	it('callAction/multiple', function() {
		let actionContainer = new ActionContainer();

		expectError(() => actionContainer.callAction());
		expectError(() => actionContainer.callAction(""));

		let action1CallCount = 0, action2CallCount = 0;
		actionContainer._actionList.push({
			name: 'test',
			callback: (param) => {
				expect(param).to.equal('test_parameter');
				action1CallCount++;
			}
		});
		actionContainer._actionList.push({
			name: 'test_2',
			callback: (param1, param2, param3) => {
				expect(param1).to.equal('test_parameter_1');
				expect(param2).to.equal('test_parameter_2');
				expect(param3).to.equal('test_parameter_3');
				action2CallCount++;
			}
		});

		actionContainer.callAction([{
			[FUNCTION_NAME]: 'test',
			[FUNCTION_PARAMETER]: ['test_parameter']
		},{
			[FUNCTION_NAME]: 'test_2',
			[FUNCTION_PARAMETER]: ['test_parameter_1', 'test_parameter_2', 'test_parameter_3']
		},{
			[FUNCTION_NAME]: 'test',
			[FUNCTION_PARAMETER]: 'test_parameter'
		}]);
		expect(action1CallCount).to.equal(2);
		expect(action2CallCount).to.equal(1);
	});

	it('getCaller', function() {
		let actionContainer = new ActionContainer();
		const actionObj = { 1:'test_action' };

		let actionCallCount = 0, preventCallCount = 0;

		actionContainer.callAction = (param) => {
			expect(param).to.equal(actionObj);
			actionCallCount++;
		};

		const callerFunc = actionContainer.getCaller(actionObj);
		expect(callerFunc).to.be.a('function');

		callerFunc({
			preventDefault: () => {
				preventCallCount++;
			}
		});

		expect(actionCallCount).to.equal(1);
		expect(preventCallCount).to.equal(1);
	});
});