import ElementContainer from '../../src/parser/ElementContainer.js';
import ElementTypes from '../../src/constants/element_types';
import ParserTypes from '../../src/constants/parser_types';

import '../../src/polyfills/index';

describe("parser/ElementContainer", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor', function() {
		let elementContainer = new ElementContainer();

		expect(elementContainer).to.have.property('_singleLineElements').to.be.an('array').to.be.empty;
		expect(elementContainer).to.have.property('_multiLineElements').to.be.an('array').to.be.empty;
	});


	const singleLineElement_1 = { types: ['headline', '2'] } ;
	const singleLineElement_2 = { types: '1' };
	const multiLineElement_1 = { types: ['2', 'headline'] };
	const multiLineElement_2 = { types: ['text', '1'] };
	const multiLineElement_3 = { types: null };

	function registerElements(elementContainer) {
		elementContainer.registerElement(singleLineElement_1, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
		elementContainer.registerElement(multiLineElement_2, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
		elementContainer.registerElement(multiLineElement_1, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
		elementContainer.registerElement(singleLineElement_2, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
		elementContainer.registerElement(multiLineElement_3, ElementTypes.ELEMENT_TYPE_MULTI_LINE);

		elementContainer.registerElement(multiLineElement_3, ElementTypes.ELEMENT_TYPE_MULTI_LINE);
		elementContainer.registerElement(singleLineElement_1, ElementTypes.ELEMENT_TYPE_SINGLE_LINE);
	}

	it('registerElement', function() {
		let elementContainer = new ElementContainer();

		registerElements(elementContainer);

		expect(elementContainer).to.have.property('_singleLineElements').to.be.an('array').to.have.lengthOf(2);
		expect(elementContainer).to.have.property('_multiLineElements').to.be.an('array').to.have.lengthOf(3);
	});

	it('getElementDefinition', function() {
		let elementContainer = new ElementContainer();

		registerElements(elementContainer);

		expect(elementContainer.getElementDefinition()).to.equal(null);

		expect(elementContainer.getElementDefinition('headline', ParserTypes.PARSE_TYPE_SINGLE_LINE)).to.equal(singleLineElement_1);
		expect(elementContainer.getElementDefinition('2', ParserTypes.PARSE_TYPE_SINGLE_LINE)).to.equal(singleLineElement_1);

		expect(elementContainer.getElementDefinition('1', ParserTypes.PARSE_TYPE_SINGLE_LINE)).to.equal(singleLineElement_2);

		expect(elementContainer.getElementDefinition('2', ParserTypes.PARSE_TYPE_MULTI_LINE)).to.equal(multiLineElement_1);
		expect(elementContainer.getElementDefinition('headline', ParserTypes.PARSE_TYPE_MULTI_LINE)).to.equal(multiLineElement_1);

		expect(elementContainer.getElementDefinition('1', ParserTypes.PARSE_TYPE_MULTI_LINE)).to.equal(multiLineElement_2);
		expect(elementContainer.getElementDefinition('text', ParserTypes.PARSE_TYPE_MULTI_LINE)).to.equal(multiLineElement_2);

		expect(elementContainer.getElementDefinition('headline', ParserTypes.PARSE_TYPE_ALL)).to.equal(singleLineElement_1);
		expect(elementContainer.getElementDefinition('1', ParserTypes.PARSE_TYPE_ALL)).to.equal(singleLineElement_2);
		expect(elementContainer.getElementDefinition('text', ParserTypes.PARSE_TYPE_ALL)).to.equal(multiLineElement_2);
	});
});