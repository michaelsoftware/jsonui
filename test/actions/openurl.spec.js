import openUrl from '../../src/actions/openUrl';

describe("actions/openUrl", function() {
	beforeEach(function() {
		openCallCount = openAlertCount = focusCount = 0;
	});
	afterEach(function() { });


	let openCallCount = 0;
	let openAlertCount = 0;
	let focusCount = 0;

	let openReturnValue;

	window.open = (url, name, specs, replace) => {
		expect(url).to.equal('test_url');
		expect(name).to.equal('_blank');
		expect(specs).to.equal(undefined);
		expect(replace).to.equal(undefined);

		openCallCount++;

		return openReturnValue;
	};

	window.alert = (title) => {
		expect(title).to.equal('Please allow popups for this website.');

		openAlertCount++;
	};


	it('openUrl (w/o alert)', function() {
		openReturnValue = {
			focus: () => focusCount++
		};

		openUrl('test_url');

		expect(openCallCount).to.equal(1);
		expect(focusCount).to.equal(1);
		expect(openAlertCount).to.equal(0);
	});

	it('openUrl (with alert)', function() {
		openReturnValue = null;

		openUrl('test_url');

		expect(openCallCount).to.equal(1);
		expect(focusCount).to.equal(0);
		expect(openAlertCount).to.equal(1);
	});
});