import {isEmpty, isFunction, isObject, isArray, isInteger, isNumeric, isString, isBoolean, isHexColor, isLocale, isRegex} from '../../src/utils/is.js';

describe("utils/is", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('isEmpty', function() {
		expect(isEmpty()).to.be.true;
		expect(isEmpty(null)).to.be.true;
		expect(isEmpty('')).to.be.true;
		expect(isEmpty(new String())).to.be.true;
		expect(isEmpty([])).to.be.true;
		expect(isEmpty(new Array())).to.be.true;
		expect(isEmpty('undefined')).to.be.true;
		expect(isEmpty('null')).to.be.true;
		expect(isEmpty('NaN')).to.be.true;

		expect(isEmpty('Hello World')).to.be.false;
		expect(isEmpty('0')).to.be.false;
		expect(isEmpty(0)).to.be.false;
		expect(isEmpty({})).to.be.false;
		expect(isEmpty(NaN)).to.be.false;
	});


	it('isFunction', function() {
		expect(isFunction(function() {})).to.be.true;
		expect(isFunction(() => {})).to.be.true;
		expect(isFunction(new Function())).to.be.true;
		expect(isFunction(alert)).to.be.true;

		expect(isFunction({})).to.be.false;
		expect(isFunction([])).to.be.false;
		expect(isFunction("")).to.be.false;
		expect(isFunction(0)).to.be.false;
		expect(isFunction(null)).to.be.false;
		expect(isFunction()).to.be.false;
	});


	it('isObject', function() {
		expect(isObject({})).to.be.true;
		expect(isObject(new Object())).to.be.true;
		expect(isObject(window)).to.be.true;
		expect(isObject(JSON.parse("{}"))).to.be.true;
		expect(isObject(new String())).to.be.true;
		expect(isObject(new Array())).to.be.true;
		expect(isObject([])).to.be.true;

		expect(isObject("")).to.be.false;
		expect(isObject(0)).to.be.false;
		expect(isObject(null)).to.be.false;
		expect(isObject()).to.be.false;
	});


	it('isArray', function() {
		expect(isArray(new Array())).to.be.true;
		expect(isArray(JSON.parse("[]"))).to.be.true;
		expect(isArray([])).to.be.true;

		expect(isArray({})).to.be.false;
		expect(isArray(new Object())).to.be.false;
		expect(isArray(window)).to.be.false;
		expect(isArray(new String())).to.be.false;
		expect(isArray("")).to.be.false;
		expect(isArray(0)).to.be.false;
		expect(isArray(null)).to.be.false;
		expect(isArray()).to.be.false;
	});


	it('isInteger', function() {
		expect(isInteger(0)).to.be.true;
		expect(isInteger(1)).to.be.true;
		expect(isInteger(-1)).to.be.true;


		expect(isInteger(new Array())).to.be.false;
		expect(isInteger(JSON.parse("[]"))).to.be.false;
		expect(isInteger([])).to.be.false;
		expect(isInteger({})).to.be.false;
		expect(isInteger(new Object())).to.be.false;
		expect(isInteger(window)).to.be.false;
		expect(isInteger(new String())).to.be.false;
		expect(isInteger("")).to.be.false;
		expect(isInteger(null)).to.be.false;
		expect(isInteger()).to.be.false;
	});


	it('isNumeric', function() {
		expect(isNumeric(0)).to.be.true;
		expect(isNumeric(1)).to.be.true;
		expect(isNumeric(-1)).to.be.true;
		expect(isNumeric("-1")).to.be.true;
		expect(isNumeric("0x539")).to.be.true;
		expect(isNumeric("0.2")).to.be.true;

		expect(isNumeric("")).to.be.false;
		expect(isNumeric("NaN")).to.be.false;
		expect(isNumeric(new Array())).to.be.false;
		expect(isNumeric(Infinity)).to.be.false;
		expect(isNumeric(null)).to.be.false;
		expect(isNumeric()).to.be.false;
	});


	it('isString', function() {
		expect(isString("-1")).to.be.true;
		expect(isString("")).to.be.true;
		expect(isString(new String())).to.be.true;
		expect(isString("Hello World")).to.be.true;
		expect(isString("NaN")).to.be.true;

		expect(isString(new Array())).to.be.false;
		expect(isString(null)).to.be.false;
		expect(isString()).to.be.false;
		expect(isString({})).to.be.false;
		expect(isString(0)).to.be.false;
	});


	it('isBoolean', function() {
		expect(isBoolean(true)).to.be.true;
		expect(isBoolean(false)).to.be.true;
		expect(isBoolean(new Boolean(true))).to.be.true;
		expect(isBoolean(new Boolean(false))).to.be.true;
		expect(isBoolean(new Object(false))).to.be.true;
		expect(isBoolean(new Object(true))).to.be.true;

		expect(isBoolean("true")).to.be.false;
		expect(isBoolean(0)).to.be.false;
		expect(isBoolean(1)).to.be.false;
		expect(isBoolean(null)).to.be.false;
		expect(isBoolean()).to.be.false;
		expect(isBoolean({})).to.be.false;
		expect(isBoolean(NaN)).to.be.false;
	});

	it('isHexColor', function() {
		expect(isHexColor("#FFFFFF")).to.be.true;
		expect(isHexColor("#FF0")).to.be.true;
		expect(isHexColor("#EE000000", true)).to.be.true;

		expect(isHexColor("#")).to.be.false;
		expect(isHexColor("#0e")).to.be.false;
		expect(isHexColor("#g8e")).to.be.false;
		expect(isHexColor("#EE000000")).to.be.false;

		expect(isHexColor("true")).to.be.false;
		expect(isHexColor(0)).to.be.false;
		expect(isHexColor(1)).to.be.false;
		expect(isHexColor(null)).to.be.false;
		expect(isHexColor()).to.be.false;
		expect(isHexColor({})).to.be.false;
		expect(isHexColor(NaN)).to.be.false;
	});

	it('isLocale', function() {
		expect(isLocale(new String("en_GB"))).to.be.true;
		expect(isLocale("DE_DE")).to.be.true;
		expect(isLocale("sw")).to.be.true;

		expect(isLocale("01234")).to.be.false;
		expect(isLocale(null)).to.be.false;
		expect(isLocale({})).to.be.false;
	});

	it('isRegex', function() {
		expect(isRegex(new RegExp('test', 'i'))).to.be.true;
		expect(isRegex(/test/i)).to.be.true;

		expect(isRegex(new String("/test/g"))).to.be.false;
		expect(isRegex("01234")).to.be.false;
		expect(isRegex(null)).to.be.false;
		expect(isRegex({})).to.be.false;
	});
});

