import ActionQueue from '../../src/utils/ActionQueue.js';

describe("utils/ActionQueue", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('constructor empty', function() {
		let actionqueue = new ActionQueue();

		expect(actionqueue).to.have.property('_queue').to.be.an('array').to.be.empty;
		expect(actionqueue).to.have.property('_running').false;
		expect(actionqueue).to.not.have.property('_iterator');
	});

	it('constructor function', function() {
		const iterator = function() {

		};
		let actionqueue = new ActionQueue(iterator);

		expect(actionqueue).to.have.property('_queue').to.be.an('array').to.be.empty;
		expect(actionqueue).to.have.property('_running').false;
		expect(actionqueue).to.have.property('_iterator').to.equal(iterator);
	});

	it('constructor wrong 1', function() {
		let actionqueue = new ActionQueue("sadsadsa");

		expect(actionqueue).to.have.property('_queue').to.be.an('array').to.be.empty;
		expect(actionqueue).to.have.property('_running').false;
		expect(actionqueue).to.not.have.property('_iterator');
	});

	it('constructor wrong 2', function() {
		let actionqueue = new ActionQueue(undefined);

		expect(actionqueue).to.have.property('_queue').to.be.an('array').to.be.empty;
		expect(actionqueue).to.have.property('_running').false;
		expect(actionqueue).to.not.have.property('_iterator');
	});


	it('undefined item', function(done) {
		let actionqueue = new ActionQueue(undefined);
		actionqueue.add(undefined);

		expect(actionqueue).to.have.property('_queue').to.be.an('array').to.have.property('length', 1);
		expect(actionqueue).to.have.property('_running').false;
		expect(actionqueue).to.not.have.property('_iterator');

		const iterator = function(object) {
			expect(object).to.equal(undefined);
			done();
		};

		actionqueue.setIterator(iterator);

		expect(actionqueue).to.have.property('_iterator').equal(iterator);
	});


	it('item list', function(done) {
		const testData = [undefined, [], [1, 2, 3], {}, "", "Hello World", 1, NaN, null];
		const testDataSize = testData.length;
		let i = 0;

		let actionqueue = new ActionQueue();
		testData.map((element) => {
			actionqueue.add(element);
		});

		expect(actionqueue).to.have.property('_queue').to.be.an('array').to.have.property('length', testDataSize);
		expect(actionqueue).to.have.property('_running').false;
		expect(actionqueue).to.not.have.property('_iterator');

		const iterator = function(object) {
			expect(actionqueue).to.have.property('_running').true;
			expect(object).to.equal(testData[i]);

			i++;

			if(testDataSize === testDataSize) done();
		};
		actionqueue.setIterator(iterator);

		expect(actionqueue).to.have.property('_iterator').equal(iterator);
	});
});

