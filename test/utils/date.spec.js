import {isLeapYear, getDaysInMonth} from '../../src/utils/date.js';

describe("utils/date", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('isLeapYear', function() {
		const leapYears = [1904,1908,1912,1916,1920,1924,1928,1932,1936,1940,1944,1948,1952,1956,1960,1964,1968,1972,1976,1980,1984,1988,1992,1996,2000,2004,2008,2012,2016,2020,2024,2028,2032,2036,2040,2044,2048,2052,2056,2060,2064,2068,2072,2076,2080,2084,2088,2092,2096];

		for(let i = 1900; i <= 2100; i++) {
			if(leapYears.indexOf(i) !== -1) {
				expect(isLeapYear(i)).to.be.true;
			} else {
				expect(isLeapYear(i)).to.be.false;
			}
		}
	});

	it('getDaysInMonth', function() {
		const testData = [{
			month: 0,
			year: 1952,
			result: 31
		},{
			month: 1,
			year: 1900,
			result: 28
		},{
			month: 2,
			year: 2015,
			result: 31
		},{
			month: 3,
			year: 2065,
			result: 30
		},{
			month: 4,
			year: 1975,
			result: 31
		},{
			month: 5,
			year: 1995,
			result: 30
		},{
			month: 6,
			year: 1935,
			result: 31
		},{
			month: 7,
			year: 2002,
			result: 31
		},{
			month: 8,
			year: 1997,
			result: 30
		},{
			month: 9,
			year: 2035,
			result: 31
		},{
			month: 10,
			year: 2017,
			result: 30
		},{
			month: 11,
			year: 2045,
			result: 31
		},{
			month: 1,
			year: 1904,
			result: 29
		},{
			month: 13,
			year: 1903,
			result: 29
		},{
			month: 13,
			year: 1904,
			result: 28
		}];

		testData.map((element) => {
			expect(getDaysInMonth(element.month, element.year)).to.be.equal(element.result);
		});
	});
});

