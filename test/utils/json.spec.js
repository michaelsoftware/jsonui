import {tryParseJSON, parseJuiJSON} from '../../src/utils/json.js';

describe("utils/json", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('tryParseJSON', function() {
		expect(tryParseJSON('{"hello":"world"}')).to.have.property('hello').equal('world');
		expect(tryParseJSON(1)).to.equal(1);

		expect(tryParseJSON()).to.equal(null);
		expect(tryParseJSON("")).to.equal(null);
		expect(tryParseJSON(null)).to.equal(null);
		expect(tryParseJSON('{"hello",:"world"}')).to.equal(null);
	});

	it('parseJuiJSON', function() {
		expect(parseJuiJSON('{"hello":"world"}')).to.have.property('hello').equal('world');
	});

	it('parseJuiJSON no content', function() {
		const parsed = parseJuiJSON(null);

		expect(parsed).to.not.equal(null);
		expect(parsed.length).to.equal(1);

		const firstElement = parsed[0];

		expect(firstElement).to.have.property('type').equal('heading');
		expect(firstElement).to.have.property('value').equal('NoContent - Sorry, but there is nothing to parse :-(');
		expect(firstElement).to.have.property('color').equal('#FF0000');
	});


	it('parseJuiJSON bad content', function() {
		const jsonString = 'Invalid JSON!!';
		const parsed = parseJuiJSON(jsonString);

		expect(parsed).to.not.equal(null);
		expect(parsed.length).to.equal(2);

		const firstElement = parsed[0];
		const secondElement = parsed[1];

		expect(firstElement).to.have.property('type').equal('heading');
		expect(firstElement).to.have.property('value').equal('Error while parsing JSON');
		expect(firstElement).to.have.property('color').equal('#FF0000');

		expect(secondElement).to.have.property('type').equal('text');
		expect(secondElement).to.have.property('value').equal(jsonString);
	});
});

