import {getDomElement} from '../../src/utils/dom.js';

describe("utils/dom", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('getDomElement by string', function() {
		let element = document.createElement('div');
		element.className = '__get_dom_element';
		document.body.appendChild(element);

		expect(getDomElement('.__get_dom_element')).to.be.equal(element);
		expect(getDomElement(new String('.__get_dom_element'))).to.be.equal(element);
	});

	it('getDomElement by reference', function() {
		let element = document.createElement('div');
		document.body.appendChild(element);

		expect(getDomElement(element)).to.be.equal(element);
	});

	it('getDomElement by wrong value', function() {
		expect(getDomElement({})).to.be.equal(null);
		expect(getDomElement("body html")).to.be.equal(null);
		expect(getDomElement("1")).to.be.equal(null);
		expect(getDomElement(1)).to.be.equal(null);
		expect(getDomElement(false)).to.be.equal(null);
		expect(getDomElement([])).to.be.equal(null);
		expect(getDomElement(new String())).to.be.equal(null);
	});
});

