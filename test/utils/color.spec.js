import {isHexColor as isHexColorIs} from '../../src/utils/is.js';
import {isHexColor, hexToRgb, hexToRgbString} from '../../src/utils/color.js';

describe("utils/color", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('isHexColor', function() {
		expect(isHexColor).to.equal(isHexColorIs);
	});

	it('hexToRgb', function() {
		expect(hexToRgb('FFF')).to.be.null;
		expect(hexToRgb('#ZFF')).to.be.null;

		expect(hexToRgb('#FFF')).to.containSubset({
			r: 255,
			g: 255,
			b: 255
		}).to.not.have.property('a');

		expect(hexToRgb('#F16096')).to.containSubset({
			r: 241,
			g: 96,
			b: 150
		}).to.not.have.property('a');

		expect(hexToRgb('#54E54A')).to.containSubset({
			r: 84,
			g: 229,
			b: 74
		}).to.not.have.property('a');
	});

	it('hexToRgb (alpha)', function() {
		expect(hexToRgb('FFF', true)).to.be.null;
		expect(hexToRgb('#ZFF', true)).to.be.null;

		expect(hexToRgb('#FFF', true)).to.containSubset({
			r: 255,
			g: 255,
			b: 255,
			a: 1,
		});

		expect(hexToRgb('#F16096', true)).to.containSubset({
			r: 241,
			g: 96,
			b: 150,
			a: 1
		});

		expect(hexToRgb('#5354E54A', true)).to.containSubset({
			r: 84,
			g: 229,
			b: 74,
			a: 0.33
		});

		expect(hexToRgb('#0054E54A', true)).to.containSubset({
			r: 84,
			g: 229,
			b: 74,
			a: 0
		});

		expect(hexToRgb('#FF54E54A', true)).to.containSubset({
			r: 84,
			g: 229,
			b: 74,
			a: 1
		});
	});

	it('hexToRgbString', function() {
		expect(hexToRgbString('FFF')).to.equal('rgb(0, 0, 0)');
		expect(hexToRgbString('#ZFF')).to.equal('rgb(0, 0, 0)');

		expect(hexToRgbString('#FFF')).to.equal('rgb(255, 255, 255)');
		expect(hexToRgbString('#F16096')).to.equal('rgb(241, 96, 150)');
		expect(hexToRgbString('#54E54A')).to.equal('rgb(84, 229, 74)');
	});

	it('hexToRgbString (alpha)', function() {
		expect(hexToRgbString('FFF', true)).to.equal('rgba(0, 0, 0, 1.00)');
		expect(hexToRgbString('#ZFF', true)).to.equal('rgba(0, 0, 0, 1.00)');

		expect(hexToRgbString('#FFF', true)).to.equal('rgba(255, 255, 255, 1.00)');
		expect(hexToRgbString('#F16096', true)).to.equal('rgba(241, 96, 150, 1.00)');

		expect(hexToRgbString('#5354E54A', true)).to.equal('rgba(84, 229, 74, 0.33)');
		expect(hexToRgbString('#0054E54A', true)).to.equal('rgba(84, 229, 74, 0.00)');
		expect(hexToRgbString('#FF54E54A', true)).to.equal('rgba(84, 229, 74, 1.00)');
	});
});

