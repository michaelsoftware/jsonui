import {getJuiProperty} from '../../src/utils/shortener.js';

describe("utils/shortener", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('getValue', function() {
		const colorObj = {
			'visibility': 'none'
		};
		const confObj = {
			'type':'1', // type:text
			'1':'text', // type:text
			'2':'Hello World!', // value:Hello World!
			'html':'<h1>Test</h1>',
			'15': colorObj,
			'-1': 0,
			'all': 0
		};

		expect(getJuiProperty('type', confObj)).to.equal('1');
		expect(getJuiProperty('1', confObj)).to.equal('text');

		expect(getJuiProperty('2', confObj)).to.equal('Hello World!');
		expect(getJuiProperty('value', confObj)).to.equal('Hello World!');

		expect(getJuiProperty('16', confObj)).to.equal('<h1>Test</h1>');
		expect(getJuiProperty('html', confObj)).to.equal('<h1>Test</h1>');

		expect(getJuiProperty('15', confObj)).to.equal(colorObj);

		expect(getJuiProperty('-1', confObj)).to.equal(0);
		expect(getJuiProperty('27', confObj)).to.equal(0);
	});
});

