import Numerous from '../../../../src/utils/lang/plugins/Numerous';

describe("utils/lang/plugins/Numerous", function() {
	beforeEach(function() { });
	afterEach(function() { });


	it('constructor', function() {
		let testNumerous = new Numerous();
		expect(testNumerous).to.have.property('regex');
		expect(testNumerous).to.have.property('keyRegex');
	});

	it('parseKey', function() {
		let testNumerous = new Numerous();

		expect(() => testNumerous.parseKey(null)).to.throw().to.have.property('name').to.equal('InvalidParameterException');
		expect(() => testNumerous.parseKey({})).to.throw().to.have.property('name').to.equal('InvalidParameterException');
		expect(() => testNumerous.parseKey('')).to.not.throw();

		const test1result = testNumerous.parseKey('');
			expect(test1result).to.have.property('key').to.equal('');
			expect(test1result).to.have.property('operator').to.equal('<');
			expect(test1result).to.have.property('value').to.equal(2);
			expect(test1result).to.have.property('default').to.equal(true);

		const test2result = testNumerous.parseKey('test <');
			expect(test2result).to.have.property('key').to.equal('test <');
			expect(test2result).to.have.property('operator').to.equal('<');
			expect(test2result).to.have.property('value').to.equal(2);
			expect(test2result).to.have.property('default').to.equal(true);

		const test3result = testNumerous.parseKey('test <= 15');
			expect(test3result).to.have.property('key').to.equal('test');
			expect(test3result).to.have.property('operator').to.equal('<=');
			expect(test3result).to.have.property('value').to.equal(15);
			expect(test3result).to.not.have.property('default');
	});

	it('test', function() {
		function testCases(inputArray, expectedOutput) {
			inputArray.map((testcase) => {
				const testObject = {
					operator: testcase[0],
					value: testcase[1]
				};

				expect( testNumerous.test(testObject, 5) ).to.equal(expectedOutput);
			});
		}

		let testNumerous = new Numerous();

		let testCasesTrue = [
			['==', 5],
			['!=', 6],
			['<' , 6],
			['>' , 4],
			['>=', 4],
			['>=', 5],
			['<=', 6],
			['<=', 5]
		];
		testCases(testCasesTrue, true);

		let testCasesFalse = [
			['==', 4],
			['!=', 5],
			['<' , 5],
			['>' , 5],
			['>=', 6],
			['<=', 4]
		];
		testCases(testCasesFalse, false);
	});

	it('onFound', function() {
		let testNumerous = new Numerous();

		/* default (true) */
		testNumerous.parseKey = () => {return {key: 'myValue', value: 2, operator: '<', default: true}};
		testNumerous.test = () => true;
		const output1 = testNumerous.onFound({myValue: 15}, '{myValue??one::more}', 'myValue', 'one', 'more');
		expect(output1).to.equal('one');

		/* default (false) */
		testNumerous.parseKey = () => {return {key: 'myValue', value: 2, operator: '<', default: true}};
		testNumerous.test = () => false;
		const output2 = testNumerous.onFound({myValue: 15}, '{myValue??one::more}', 'myValue', 'one', 'more');
		expect(output2).to.equal('more');


		/* custom (true) */
		testNumerous.parseKey = () => {return {key: 'myValue', value: 15, operator: '==', default: true}};
		testNumerous.test = () => true;
		const output3 = testNumerous.onFound({myValue: 15}, '{myValue == 15??one::more}', 'myValue == 15', 'true', 'false');
		expect(output3).to.equal('true');

		/* custom (false) */
		testNumerous.parseKey = () => {return {key: 'myValue', value: 15, operator: '!=', default: true}};
		testNumerous.test = () => false;
		const output4 = testNumerous.onFound({myValue: 15}, '{myValue != 15??one::more}', 'myValue != 15', 'true', 'false');
		expect(output4).to.equal('false');


		/* custom (false/empty) */
		testNumerous.parseKey = () => {return {key: 'myValue', value: 15, operator: '!=', default: true}};
		testNumerous.test = () => false;
		const output5 = testNumerous.onFound({myValue: 15}, '{myValue != 15??one::}', 'myValue != 15', 'true');
		expect(output5).to.be.not.ok;
	});
});