import Replacer from '../../../../src/utils/lang/plugins/Replacer';

describe("utils/lang/plugins/Replacer", function() {
	beforeEach(function() { });
	afterEach(function() { });


	it('constructor', function() {
		let testReplacer = new Replacer();
		expect(testReplacer).to.have.property('regex');
	});

	it('onFound', function() {
		const conf = {
			hello: 'Hello World'
		};
		let testReplacer = new Replacer();

		expect( testReplacer.onFound(conf, '##hello##', 'hello', 0, '##hello##!') ).to.equal('Hello World');

		expect( testReplacer.onFound(conf, '##username##', 'username', 0, 'Hello ##username##!') ).to.not.be.ok;
	});
});