import LanguageHelper from '../../../src/utils/lang/LanguageHelper.js';
import Locales from '../../../src/utils/lang/Locale.js';

describe("utils/lang/LanguageHelper", function() {
	beforeEach(function() {
		LanguageHelper.clear();
	});
	afterEach(function() { });

	it('getLocale/setLocale', function() {
		expect(LanguageHelper.getLocale()).to.equal(Locales.EN_GB);

		expect(function() {
			LanguageHelper.setLocale();
		}).to.throw().to.have.property('name').to.equal('InvalidParameterException');

		expect(LanguageHelper.getLocale()).to.equal(Locales.EN_GB);

		LanguageHelper.setLocale('de_DE');

		expect(LanguageHelper.getLocale()).to.equal(Locales.DE_DE);

		LanguageHelper.setLocale('de');

		expect(LanguageHelper.getLocale()).to.equal('de');

		expect(function() {
			LanguageHelper.setLocale('abc');
		}).to.throw().to.have.property('name').to.equal('InvalidParameterException');
	});

	it('loadCollectionData', function() {
		expect(function() {
			LanguageHelper.loadCollectionData();
		}).to.throw().to.have.property('name').to.equal('InvalidParameterException');

		expect(function() {
			LanguageHelper.loadCollectionData(null, 'Test', {});
		}).to.throw().to.have.property('name').to.equal('InvalidParameterException');

		expect(function() {
			LanguageHelper.loadCollectionData('en_GB', 'Test2', null);
		}).to.throw().to.have.property('name').to.equal('InvalidParameterException');

		expect(function() {
			LanguageHelper.loadCollectionData(Locales.EN_GB, null, {});
		}).to.throw().to.have.property('name').to.equal('InvalidParameterException');

		LanguageHelper.loadCollectionData(Locales.DE_DE, 'testcollection', {
			'txt_hello_world': 'Hello World!'
		});
	});

	it('get', function() {
		LanguageHelper.loadCollectionData(Locales.DE_DE, 'testcollection', {
			'txt_hello_world': 'Hallo Welt!'
		});

		LanguageHelper.loadCollectionData(Locales.EN_GB, 'testcollection', {
			'intro': {
				'txt_hello_world': 'Hello world!'
			}
		});

		expect(LanguageHelper.get('testcollection')).to.equal(null);

		expect(LanguageHelper.get('testcollection.txt_hello_world')).to.equal(null);

		LanguageHelper.setLocale(Locales.DE_DE);

		expect(LanguageHelper.get('testcollection.txt_hello_world')).to.equal('Hallo Welt!');

		LanguageHelper.setLocale(Locales.EN_GB);

		expect(LanguageHelper.get('testcollection.intro')).to.equal(null);
		expect(LanguageHelper.get('testcollection.intro.txt_hello_world')).to.equal('Hello world!');
	});

	it('plugins/default_behavior', function() {
		LanguageHelper.loadCollectionData(Locales.EN_GB, 'testcollection', {
			'txt_hello_world': 'HelloWorld!',
			'txt_hello_world_var': 'Hello ##world##!'
		});

		let testPlugin = new TestPlugin();
		LanguageHelper.addPlugin(testPlugin);


		expect(LanguageHelper.get('testcollection')).to.equal(null);
		expect(testPlugin.getCallCount()).to.equal(0);

		expect(LanguageHelper.get('testcollection.txt_hello_world')).to.equal('HelloWorld!');
		expect(testPlugin.getCallCount()).to.equal(1);

		testPlugin.clear();

		expect(LanguageHelper.get('testcollection.txt_hello_world_var')).to.equal('Hello ##world##!');
		expect(testPlugin.getCallCount()).to.equal(2);
	});

	it('plugins/replace', function() {
		LanguageHelper.loadCollectionData(Locales.EN_GB, 'testcollection', {
			'txt_hello_world': 'HelloWorld!',
			'txt_hello_world_var': 'Hello ##world##!'
		});

		let testPlugin = new TestPlugin();
		LanguageHelper.addPlugin(testPlugin);

		testPlugin.setOnFoundListener((conf, match) => {
			if(match === 'world')
				return '123';
		});

		expect(LanguageHelper.get('testcollection.txt_hello_world_var')).to.equal('Hello ##123##!');
		expect(testPlugin.getCallCount()).to.equal(2);




		const textConf = {
			alpha: 123
		};
		testPlugin.setOnFoundListener((conf) => {
			expect(conf).to.equal(textConf);

			return '123';
		});
		expect(LanguageHelper.get('testcollection.txt_hello_world_var', textConf)).to.equal('123 ##123##!');
	});
});




import ReplacePlugin from '../../../src/utils/lang/ReplacePlugin.js';

class TestPlugin extends ReplacePlugin {
	constructor() {
		super(/[a-zA-Z]+/g);

		this.i = 0;
	}

	onFound(...params) {
		this.i++;

		if(this.callback) return this.callback(...params);
	}

	clear() {
		this.i = 0;
	}

	getCallCount() {
		return this.i;
	}

	setOnFoundListener(callback) {
		this.callback = callback;
	}
}