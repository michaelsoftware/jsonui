import ReplacePlugin from '../../../src/utils/lang/ReplacePlugin.js';

describe("utils/lang/ReplacePlugin", function() {
	beforeEach(function() {
	});
	afterEach(function() { });

	function testThrow(functionToCall) {
		expect(functionToCall).to.throw().to.have.own.property('name').to.equal('InvalidParameterException');
	}

	it('constructor', function() {
		testThrow(() => {
			new ReplacePlugin();
		});

		testThrow(() => {
			new ReplacePlugin("hello");
		});


		const regex = /[a-z]+/;
		let testReplace = new ReplacePlugin(regex);
		expect(testReplace).to.have.own.property('regex').to.equal(regex);
	});

	it('process', function() {
		let count = 0;
		const testObject = {alpha:123};

		let testReplace = new ReplacePlugin(/[a-z]+/g);
		testReplace.onFound = (...params) => {
			expect(params).to.have.lengthOf(4);
			expect(params).to.have.property(0).to.equal(testObject);

			count++;
		};

		testThrow(() => {
			testReplace.process({}, {});
		});

		testThrow(() => {
			testReplace.process("", "");
		});

		testReplace.process("#abc#", testObject);
		expect(count).to.equal(1);

		count = 0;

		testReplace.process("#abc#def#ghi#", testObject);
		expect(count).to.equal(3);
	});
});