import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const ROOT_PATH = path.resolve('./');

module.exports = {
	entry: {
		main: [
			path.resolve(ROOT_PATH, 'examples', 'index')
		]
	},
	output: {
		path: path.join(ROOT_PATH, 'build'),
		filename: 'bundle.js'
	},
	resolve: {
		extensions: ['.js']
	},
	devServer: {
		host: '0.0.0.0',
		port: 8081,
		compress: true,
		hot: true
	},
	devtool: 'inline-source-map',
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: path.resolve(ROOT_PATH, 'src', 'index.html')
		})
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				use: [{
					loader: 'babel-loader'
				}],
				include: path.resolve(ROOT_PATH, 'src')
			}
		]
	},
};