import {utils, Parser, constants, config, actions} from '../src/index';


let perf1 = performance.now();

config.init({
	langCode: 'en_GB'
});

window.jsonui = {
	utils,
	constants,
	config
};

let juiParser = new Parser();

juiParser.actions.addAction('submit', (url) => {
	console.log('submit()', url, window.submit());
});

juiParser.actions.addAction('openUrl', actions.openUrl);

juiParser.parse({
	body: [
		{
			[constants.keys.type]: constants.values.type.headline,
			[constants.keys.value]: "Hello World",
			[constants.keys.style]: {
				[constants.keys.margin]: {
					[constants.keys.top]: 15
				},
				[constants.keys.padding]: {
					[constants.keys.left]: 15
				},
				[constants.keys.color]: "#FF00FF",
				[constants.keys.background]: "#80000000",
			}
		},{
			[constants.keys.type]: constants.values.type.hline
		},{
			[constants.keys.type]: constants.values.type.text,
			[constants.keys.value]: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
		},{
			[constants.keys.type]: constants.values.type.text,
			[constants.keys.value]: " At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
		},{
			[constants.keys.type]: constants.values.type.nline
		},{
			[constants.keys.type]: constants.values.type.input,
			[constants.keys.name]: 'test_input',
			[constants.keys.label]: "InputLabel",
			[constants.keys.style]: {
				[constants.keys.width]: '100%'
			},
			[constants.keys.value]: "First input",
			[constants.keys.placeholder]: "my placeholder"
		},{
			[constants.keys.type]: constants.values.type.textarea,
			[constants.keys.name]: 'test_textarea',
			[constants.keys.value]: "First input",
			[constants.keys.label]: "TextareaLabel",
			[constants.keys.placeholder]: "my placeholder"
		},{
			[constants.keys.type]: constants.values.type.checkbox,
			[constants.keys.name]: 'test_checkbox',
			[constants.keys.label]: "CheckboxLabel",
			[constants.keys.value]: true
		},{
			[constants.keys.type]: constants.values.type.range,
			[constants.keys.name]: 'test_range',
			[constants.keys.label]: "RangeLabel",
			[constants.keys.value]: 15,
			[constants.keys.max]: 20
		},{
			[constants.keys.type]: constants.values.type.numberinput,
			[constants.keys.name]: 'test_number_input',
			[constants.keys.label]: "NumberLabel",
			[constants.keys.value]: -15.6
		},{
			[constants.keys.type]: constants.values.type.passwordinput,
			[constants.keys.name]: 'test_password',
			[constants.keys.label]: "PasswordLabel",
			[constants.keys.value]: 'secure password'
		},{
			[constants.keys.type]: constants.values.type.select,
			[constants.keys.name]: 'test_select',
			[constants.keys.label]: "SelectLabel",
			[constants.keys.value]: [{
				[constants.keys.value]: 'test',
				[constants.keys.text]: 'Hello World!'
			},
			"Single String",{
				[constants.keys.text]: 'Only text'
			},
			{}]
		},{
			[constants.keys.type]: constants.values.type.list,
			[constants.keys.value]: [{
				[constants.keys.value]: 'test',
				[constants.keys.click]: {
					[constants.actions.FUNCTION_NAME]: 'openUrl',
					[constants.actions.FUNCTION_PARAMETER]: ['http://example.org/']
				},
			},
			"Single String",
			{}]
		},{
			[constants.keys.type]: constants.values.type.button,
			[constants.keys.value]: "MyButton",
			[constants.keys.click]: {
				1: 'submit'
			}
		},{
			[constants.keys.type]: constants.values.type.file,
			[constants.keys.multiple]: true,
			[constants.keys.label]: "FileLabel",
			[constants.keys.name]: 'test'
		},{
			[constants.keys.type]: constants.values.type.container,
			[constants.keys.value]: [
				{
					[constants.keys.type]: constants.values.type.text,
					[constants.keys.value]: " At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
				},{
					[constants.keys.type]: constants.values.type.file,
					[constants.keys.multiple]: true,
					[constants.keys.name]: 'test_2'
				},{
                    [constants.keys.type]: constants.values.type.button,
                    [constants.keys.value]: 'Send (from within container)',
                    [constants.keys.click]: {
                        1: 'submit'
                    }
                }
			]
		},{
			[constants.keys.type]: constants.values.type.table,
			[constants.keys.value]: [{
				[constants.keys.value]: [
					{
						[constants.keys.value]: [{
							[constants.keys.type]: constants.values.type.text,
							[constants.keys.value]: " At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
						},{
                            [constants.keys.type]: constants.values.type.button,
                            [constants.keys.value]: 'Send (from within table)',
                            [constants.keys.click]: {
                                1: 'submit'
                            }
                        }]
					},
					{
						[constants.keys.value]: [{
							[constants.keys.type]: constants.values.type.text,
							[constants.keys.value]: " At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
						},{
							[constants.keys.type]: constants.values.type.nline
						},{
							[constants.keys.type]: constants.values.type.text,
							[constants.keys.value]: " At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
						}]
					}
				]
			}]
		},{
			[constants.keys.type]: constants.values.type.image,
			[constants.keys.value]: "https://dummyimage.com/600x400/000/fff"
		}
	]
}, (result) => {
	let perf2 = performance.now();
	console.info('render-time', perf2 - perf1);

	document.querySelector('#root').appendChild(result.getViews());

	window.submit = () => {
		console.log(result.submit());
	};

	result.addOnSubmitListener(console.log);

	console.log(result);
});