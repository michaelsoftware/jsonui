# jsonUI

[![experimental](https://img.shields.io/badge/stability-experimental-orange.svg?style=flat-square)](https://gitlab.com/michaelsoftware/jsonui/) 
[![npm-version](https://img.shields.io/npm/v/jsonui.svg?style=flat-square)](https://www.npmjs.com/package/jsonui/) 
[![npm-downloads](https://img.shields.io/npm/dm/jsonui.svg?style=flat-square)](https://www.npmjs.com/package/jsonui/) 

The jsonUI is more a **proof of concept** than something you could or should use in a production
environment. It is more for small private projects that wants to try something new.

Personally I have used the jsonUI (or jUI) since over a year within my private homeserver-project.
Certainly not in the current form (it was a mess and written in ES5).

jsonUI is a framework to create a user interface mainly with json.
It is planned, that you could send your jsonUI besides the normal data of your application.
jsonUI could use something like minfied json to reduce the overhead.

To have all the freedom you need, jsonUI allows you to define custom elements and
add them to the parser (the default elements also use these methods).

Currently only the parser is ready for NPM yet but a creator (mainly for the backend) will
be added to this project too.


## Installation
```
npm install -S jsonui@latest
```

## Features
* Generate your frontend at the backend (coming soon)
* Mix your UI with your normal API-data
* Small size (small overhead)
* Customizable (use CSS or completely custom elements)
* The same UI could be used on multiple platforms (assumption: a parser is written for it)
* Could speed up simple single page-applications
* It should not be possible to break your style with user generated content (content needs to be written in jsonUI, too)


## polyfills
jsonUI is written in ES6 and needs to be "compiled" by babel to work in all browsers.
You maybe have to polyfill methods in some browsers. jsonUI has some basic polyfills that
it needs to work correctly. When you need them, you have to import them from src/polyfills (or lib/polyfills)
or use your own ones. The polyfills are only needed in some older browsers.

This is the list of all polyfills that you maybe need:
* Array.find
* Object.assign

## Getting started
You have to import the parser first:
``` javascript
import {Parser} from 'jsonui';
```
Than you could create your first jsonUI-object. We will create a headline with the text
**Hello World**.
```javascript
const jui = {
    body: [{
        type: 'headline',
        value: 'Hello World'
    }]
}
```
The jsonUI-Parser can parse this object to a JuiDocument. Moreover the jsonUI-Parser could
handle a json-string, that it parses automatically. The JuiDocument is only a wrapper to
add listeners that handles actions (e.g. submit). You could append the views from it to
the dom:
```javascript
let juiParser = new Parser();

juiParser.parse(jui, (juiDocument) => {
    const views = juiDocument.getViews();
	
    document.body.appendChild(views);
});
```

### setting 
Currently some elements needs to get some texts from the language-files (already build-in).
You have to set it up once:
```javascript
import {config} from 'jsonui';

config.init({
    langCode: 'en_GB'
});
```

### minified jsonUI
The jui-object from the last example could be rewritten using the jsonUI-constants. You
need to import them too (or use the numbers directly, which is not recommended).
```javascript
import {constants} from 'jsonui';

const jui = {
    body: [{
        [constants.keys.type]: constants.values.type.headline,
        [constants.keys.value]: "Hello World",
    }]
}
```


### more details
I can't describe everything that you could do with jsonUi here. This are only some basics.
I would recommend developers that wants to develop own elements to have a closer look on the
_src/elements_ directory.


## good to know
### webpack
webpack is currently only used as a dev-server. It serves the example, with which you
could develop new components easily.

### jsdoc
Most parts of the project (the most important parts) are commented using the jsdoc-syntax.
If you have installed jsdoc globally on your computer you could run ``npm run jsdoc`` to
generate a jsdoc in the directory _jsdoc_ with the newest definitions.

### eslint
There is an eslint config in this project. You could check the source code with ``npm run eslint``.

### karma, mocha, chai
jsonUI uses karma, mocha and chai to test the project for errors. It can not guarantee that
everything work as expected, but it helps to prevent big mistakes.
The project could be tested using ``npm run test`` for a single run test or
``npm run test:dev`` for a test that watches for changes.